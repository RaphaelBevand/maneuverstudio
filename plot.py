"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

import matplotlib.pyplot as plt
from itertools import islice
from matplotlib.lines import Line2D
import csv
import numpy as np
L = 272.986
B = 32.198
XG = -5.43480691336417

# simple ship
# first and last point is aft at midship (rudder attaches here)
ship = np.asarray([[
    -0.5 * L, -0.5 * L, 0.5 * L - 0.5 * B, 0.5 * L, 0.5 * L - 0.5 * B,
    -0.5 * L, -0.5 * L], [0., 0.5 * B, 0.5 * B, 0., -0.5 * B, -0.5 * B, 0.]])

# adjust ship coordinates by cog of ship (xg)
ship += [[XG], [0.]]

# end point of rudder
rudder = np.asarray([[-B / 2], [0]])


def load(file):
    f = islice(open(file, 'r'), 14, None)
    reader = csv.DictReader(f, delimiter=' ')
    data = {k: [] for k in reader.fieldnames}
    for row in reader:
        for k in data:
            data[k].append(float(row[k]))
    return data


series = load('state.log')
psi_max = 405
n = 9
psi_diff = 45  # (psi_max)/(n-1)
deg2rad = np.pi / 180.


def get_ship(n, psi_diff):
    for i in range(0, n):
        pos = next(j for j, psi in enumerate(
            series['psi']) if abs(series['psi'][j]) >= i*psi_diff)
        # # Use the first point before target psi is reached (except for origin).
        # # This way the points are the first point before the target psi.
        # if pos != 0:
        #     pos -= 1

        x = series['x0'][pos]
        y = series['y0'][pos]

        u = np.asarray([[series['u'][pos]], [-1. * series['v'][pos]]])
        u_abs = np.sqrt(u[0][0]*u[0][0]+u[1][0]*u[1][0])
        u = np.dot(u, 0.5 * L / u_abs)

        psi = series['psi'][pos] * deg2rad
        delta = series['delta'][pos] * deg2rad

        # rotation matrices
        rotS = np.asarray([[np.cos(psi),
                            -np.sin(psi)],
                           [np.sin(psi),
                            np.cos(psi)]])

        rotR = np.asarray([[np.cos(delta),
                          -np.sin(delta)],
                          [np.sin(delta),
                           np.cos(delta)]])

        s = rotS @ ship  # rotate ship by psi
        r = rotS @ rotR @ rudder  # rotate (rotate rudder by delta) by psi
        r = np.add(r, [[s[0][0]], [s[1][0]]])
        s = np.c_[s, r]  # attach the rudder to the ship
        s = np.add(s, [[x], [y]])  # move ship to target location
        u = rotS @ u

        yield (s, u, x, y)


# copied from program
results = {'i90': 6865, 'psi90': -90.00986272268516, 't90': 227.67872583695382, 'x90': 1261.505298709082, 'y90': -841.2657585476614, 'i180': 13667, 'psi180': -180.00221459498098, 't180': 453.26804748912997, 'x180': 431.54189424997554, 'y180': -1898.1263556950553, 'i_x0_max': 7381, 'x0_max': 1268.57974724856, 'x0_max_y': -956.5114400819434, 'i_y0_max': 14200, 'y0_max': -1905.1162761917685, 'y0_max_x': 316.1486096233046, 'u': 12.59005765952299, 'R': 957.0741524251097, 'r': -0.3905851059510357, 'beta': -6.916921178287433, 'time': 2757.051734531328}

R = results['R']

psi90 = results['psi90']
t90 = results['t90']
x90 = results['x90']
y90 = results['y90']

psi180 = results['psi180']
t180 = results['t180']
x180 = results['x180']
y180 = results['y180']

x0_max_x = results['x0_max']
x0_max_y = results['x0_max_y']

y0_max_x = results['y0_max_x']
y0_max_y = results['y0_max']

# steady radius
iR0 = next(j for j, psi in enumerate(
    series['psi']) if abs(series['psi'][j]) >= psi_max)
iR1 = next(j for j, psi in enumerate(
    series['psi']) if abs(series['psi'][j+iR0]) >= psi_max+180)
iR1 += iR0

xR0 = series['x0'][iR0]
yR0 = series['y0'][iR0]
xR1 = series['x0'][iR1]
yR1 = series['y0'][iR1]
centerX = 0.5*(xR0+xR1)
centerY = 0.5*(yR0+yR1)

# path of ship
x = [x for i, x in enumerate(series['x0']) if abs(series['psi'][i]) <= psi_max]
y = [y for i, y in enumerate(series['y0']) if abs(series['psi'][i]) <= psi_max]

fig = plt.figure()
ax = fig.add_subplot(111)
ax.axis('equal')
ax.minorticks_on()
ax.grid(which='major', ls=':', lw='0.25', color='black')
ax.grid(which='minor', ls=':', lw='0.25', color='black')
ax.set_xlabel('Distance X0 [m]')
ax.set_ylabel('Distance Y0 [m]')
#
# dashed lines
#
plt.plot([-0.5*R, x0_max_x+0.5*L], [0., 0.], 'k--', lw=1.)
plt.plot([x90, x90], [0., y90], 'k--', lw=1.)
plt.plot([x180, x180], [0., y180], 'k--', lw=1.)
plt.plot([x0_max_x, x0_max_x], [0., x0_max_y], 'k--', lw=1.)
plt.plot([y0_max_x, y0_max_x], [0., y0_max_y], 'k--', lw=1.)
plt.plot([xR0, centerX], [yR0, centerY], 'k--', lw=1.)

# plot path of ship
line, = ax.plot(x, y, 'b-', lw=0.75)

# plot ships
for (s, u, x, y) in get_ship(n, psi_diff):
    plt.plot(s[0], s[1], 'k', lw=1)
    plt.plot(x, y, 'kx', markersize=4)
    plt.plot(x, y, 'ko', markersize=3)
    ax.arrow(
        x, y, u[0][0], u[1][0], head_width=0.05 * L, head_length=0.125 * L,
        width=0.1, length_includes_head=True, fill=True,
        color='g', ls='-')

# plot results on top of everything
bbox = dict(boxstyle="round", fc="0.85", lw=0.75)
arrowprops = dict(
    arrowstyle="->",
    connectionstyle="angle3,angleA=90,angleB=-180")

text_start = 'Start of maneuver'
ax.annotate(text_start, xy=(0., 0.), xytext=(-0.25*R, -0.25*R),
            bbox=bbox, arrowprops=arrowprops)

plt.plot(xR0, yR0, 'ro', markersize=2.5)
plt.plot(centerX, centerY, 'ro', markersize=2.5)
ax.annotate("Radius:\nR = {: <6.1f} m".format(results['R']),
            xy=(0.5*(xR0+centerX), 0.5*(yR0+centerY)),
            xytext=(0.5*(xR0+centerX), 0.5*(yR0+centerY)-0.35*R),
            bbox=bbox, arrowprops=arrowprops)

plt.plot(x90, y90, 'ro', markersize=3.5)
text_psi90 = \
    'Ψ = {: <6.1f} °\nt = {: <6.1f} s\nX0 = {: <6.1f} m\nY0 = {: <6.1f} m'. \
    format(psi90, t90, x90, y90)
ax.annotate(text_psi90,
            xy=(x90, y90), xytext=(x90+0.1*R, y90+0.1*R),
            bbox=bbox, arrowprops=arrowprops)

plt.plot(x180, y180, 'ro', markersize=3.5)
text_psi180 = \
    'Ψ = {: <6.1f} °\nt = {: <6.1f} s\nX0 = {: <6.1f} m,\nY0 = {: <6.1f} m'. \
    format(psi180, t180, x180, y180)
ax.annotate(text_psi180,
            xy=(x180, y180), xytext=(x180, y180+0.25*R),
            bbox=bbox, arrowprops=arrowprops)


plt.plot(x0_max_x, x0_max_y, 'ro', markersize=3.5)
text_x0_max = 'Maximum Advance:\nX0 = {: <6.1f} m'.format(x0_max_x)
ax.annotate(text_x0_max,
            xy=(x0_max_x, x0_max_y), xytext=(x0_max_x+0.1*R, x0_max_y-0.15*R),
            bbox=bbox, arrowprops=arrowprops)

plt.plot(y0_max_x, y0_max_y,  'ro', markersize=3.5)
text_y0_max = 'Maximum Transfer:\nY0 = {: <6.1f} m'.format(y0_max_y)
ax.annotate(text_y0_max,
            xy=(y0_max_x, y0_max_y), xytext=(y0_max_x-0.5*R, y0_max_y+0.25*R),
            bbox=bbox, arrowprops=arrowprops)

legend_elements = [Line2D([0], [0], color='b', lw=2, label='Path of CoG'),
                   Line2D([0], [0], color='g', lw=2, label='Velocity Vector')]
ax.legend(handles=legend_elements)

plt.show()
