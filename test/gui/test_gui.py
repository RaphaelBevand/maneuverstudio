from tkinter import Tk
import maneuverstudio.config as config
from maneuverstudio.gui.simulation import Simulation
from maneuverstudio.gui.gui import GUI


def test_gui():
    root = Tk()
    simulation = Simulation(config.PARAMETER)
    GUI(root, simulation)
