import maneuverstudio.config as config
from maneuverstudio.ship import Ship
from maneuverstudio.solver import Solver

coeff = [
    [0, -1320, 1179, 0, 0, 0, 0, -1355, 0, 0, 0, 0, 0, -151, 0, 0, 0, 0, 0, -696, 0, 213, 0, 0, 0, -207, -2463, -3865, 0, 0, -470, -447, 0, 0, 0, 0, 3175, 0, 0, 0, 611, 0, 0, -340, 0, 0],
    [0, -33, 0, 0, 0, 0, -8470, 0, 0, -47566, -6755, -10301, 2840, 85, -1945, 1731, -222, -63, 1660, 0, 0, -99, -1277, 0, 0, 0, 0, 0, 2430, 4769, 0, 0, 0, 0, 0, 0, 0, -31214, -33237, 0, 0, -4668, 13962, 0, 0, 2438],
    [0, 8, 0, 0, 0, 0, -3800, 0, -23865, 8103, 239, 5025, -1960, 0, -729, -1784, -401, 132, -793, 0, 0, 0, 652, 0, -2179, 0, 0, 0, -473, 0, 0, 0, 0, 0, 0, 0, 0, -4586, -27858, 0, 0, 1418, -6918, 0, -404, -1096]]


def test_ship():
    ship = Ship(config.PARAMETER['ship'])

    # test ship data of 'Container'
    assert ship.data['length'] == 272.986
    assert ship.data['lambda'] == 34.
    assert ship.data['u0'] == 16.0

    assert ship.data_dash['length'] == 1.0
    assert round(ship.data_dash['displacement'], 15) == 0.006399200000000
    assert round(ship.data_dash['xg'], 15) == -0.019908738592324
    assert round(ship.data_dash['izz'], 15) == 3.288000000000000e-04
    assert ship.data_dash['u0'] == 1.0
    assert round(ship.data_dash['delta_rate'], 15) == 1.331334279093942

    # test hydrodynamic coeffficients
    assert ship.coeff['cx'] == coeff[0]
    assert ship.coeff['cy'] == coeff[1]
    assert ship.coeff['cn'] == coeff[2]


def test_param_in_solver():
    ship = Ship('Container')
    solver = Solver(ship, 0.001)

    assert round(solver.mass, 15) == 0.006399200000000
    assert round(solver.izz, 15) == 3.288000000000000e-04
    assert round(solver.xg, 15) == -0.019908738592324
    assert solver.u0 == 1.0

    for i in range(0, 46):
        assert solver.cx[i] == coeff[0][i]*1.e-6
        assert solver.cy[i] == coeff[1][i]*1.e-6
        assert solver.cn[i] == coeff[2][i]*1.e-6
