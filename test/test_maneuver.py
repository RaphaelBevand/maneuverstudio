import maneuverstudio.config as config
from maneuverstudio.maneuver import Maneuver

expect = [{
    'psi90': -90.00536549659647,
    't90': 262.1373123110474,
    'x90': 1473.1102491156598,
    'y90': -1024.3240581191467,
    'psi180': -179.99593879426334,
    't180': 513.4296219491398,
    'x180': 468.94263893367423,
    'y180': -2257.7404559584293,
    'x0_max': 1479.117497966558,
    'x0_max_y': -1140.0619444539936,
    'y0_max': -2263.746592262079,
    'y0_max_x': 352.5989196489193,
    'u': 13.44919627418356,
    'R': 1130.0964240768094,
    'r': -0.35265216833243335,
    'beta': -5.897397015798615,
    'time': 3065.1227737606605
}, {
    'psi90': 90.00405517868505,
    't90': 262.9001106641794,
    'x90': 1475.9509886925805,
    'y90': 1030.0766314891584,
    'psi180': 180.00394813940804,
    't180': 515.419530696439,
    'x180': 462.62529651845745,
    'y180': 2271.001190074205,
    'x0_max': 1481.859486011397,
    'x0_max_y': 1145.3109721287938,
    'y0_max': 2276.8783594182128,
    'y0_max_x': 347.0902390992642,
    'u': 13.494192499885619,
    'R': 1138.54889731410,
    'r': 0.35115617199543603,
    'beta': 5.819433338884895,
    'time': 3077.990850326597
}]

parameter = config.PARAMETER


def test_dummy():
    m = Maneuver(parameter)
    m.tStep_max = 100  # reduce test time
    m.run()
    m.evaluate()


def test_steady_ahead():
    parameter['maneuver'] = 'Steady Ahead'
    m = Maneuver.new(parameter)
    m.tStep_max = 100  # reduce test time
    m.run()
    m.evaluate()


def test_turning_circle():
    parameter['maneuver'] = 'Turning Circle'

    # 20 deg. Turning Circle of Container ship.
    parameter['ship'] = 'Container'
    parameter['rudder_angle'] = 20.
    m = Maneuver.new(parameter)
    m.run()
    m.evaluate()

    print('Container Turning Circle')
    print(m.results)
    for k in m.results.keys():
        if not k == 'time':
            e = expect[0][k]
            r = m.results[k]
            print(k, "- expected: ", e, ", obtained:", r)
            assert abs(e - r) < 1e-10

    # same test with negative rudder angle
    parameter['ship'] = 'Container'
    parameter['rudder_angle'] = -20.
    m = Maneuver.new(parameter)
    m.run()
    m.evaluate()

    print('Container Turning Circle, -delta_max')
    print(m.results)
    for k in m.results.keys():
        if not k == 'time':
            e = expect[1][k]
            r = m.results[k]
            print(k, "- expected: ", e, ", obtained:", r)
            assert abs(e - r) < 1e-10


def test_zig_zag():
    parameter['maneuver'] = 'Zigzag'
    parameter['ship'] = 'Container'
    parameter['rudder_angle'] = 20.
    parameter['course_angle'] = 20.
    m = Maneuver.new(parameter)
    m.run()
    m.evaluate()

    parameter['ship'] = 'Container'
    parameter['rudder_angle'] = -20.
    parameter['course_angle'] = 20.
    m = Maneuver.new(parameter)
    m.run()
    m.evaluate()
