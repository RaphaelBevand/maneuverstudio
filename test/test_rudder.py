from math import pi
import maneuverstudio.config as config
from maneuverstudio.maneuver import TurningCircle


def test_rudder():
    parameter = config.PARAMETER
    parameter['Ship'] = 'Container'
    parameter['rudder_angle'] = 20
    m = TurningCircle(parameter)

    n = int((m.delta_max-m.delta_zero)/m.delta_rate/m.dt)
    for i in range(0, n):
        m.rudder()

    assert m.state.delta*180/pi <= 20.0
    m.rudder()
    assert m.state.delta*180/pi >= 20.0

    # verify target rudder angle
    m.rudder()
    assert m.state.delta*180/pi == 20.0

    parameter['ship'] = 'Tanker'
    m = TurningCircle(parameter)

    n = int((m.delta_max-m.delta_zero)/m.delta_rate/m.dt)
    for i in range(0, n):
        m.rudder()

    assert m.state.delta*180/pi <= 20.0
    m.rudder()
    assert m.state.delta*180/pi >= 20.0
