from math import pi

from maneuverstudio.ship import Ship
from maneuverstudio.state import State
from maneuverstudio.solver import Solver

# These values are from our own Matlab program.
expect = '0.000000, -15.158207, -33.371700, -4.241808, 0.993121, 0.019536, -0.044276, -0.000000, 0.000000, -0.000000, 0.000006, -0.000281, 0.000006, 0.017453'


def test_solver():
    """ The `expect` value is based on Container ship simulation. """

    ship = Ship('Container')
    solver = Solver(ship, 0.001)

    state = State(ship)
    state.delta = 1. * pi / 180.

    for i in range(0, 100000):
        state = solver.nextStep(state)

    assert repr(state) == expect
