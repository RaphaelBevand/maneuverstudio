"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

import numpy
import time
import logging

from threading import Event

import maneuverstudio.config as config
from maneuverstudio.ship import Ship
from maneuverstudio.state import State
from maneuverstudio.solver import Solver

logger = logging.getLogger(__name__)


class Maneuver(object):
    """
    Main maneuver class.

    Specific maneuvers inherit from this class.
    An inherited class sets its own `rudder()` and `evaluate()` function.

    :param parameter:
        Parameter for maneuver/computation.
    """

    name = 'undefined'

    def __init__(self, parameter):

        logger.debug("Initializing maneuver '{}'".format(self.name))

        self.parameter = parameter

        self.dt = self.parameter['tstepsize']
        self.tStep_max = int(self.parameter['time_max'] / self.dt)

        self.realtime = self.parameter['realtime']

        self.delta_max = self.parameter['rudder_angle'] * numpy.pi/180.
        self.psi_max = self.parameter['course_angle'] * numpy.pi/180.

        # initialize ship
        self.ship = Ship(parameter['ship'])

        self.delta_zero = self.ship.data_dash['delta_zero']
        self.delta_rate = self.ship.data_dash['delta_rate']

        # initialize the state of the ship
        self.state = State(self.ship)

        # initialize solver
        self.solver = Solver(self.ship, self.dt)

        # store results of evaluation
        # values are asigned at the end of `evaluate()`
        # check the specific maneuver classes
        self.results = dict()

        logger.info("Manuever '{}' initialized". format(self.name))

    def rudder(self):
        """
        Default dummy rudder function.
        Specific maneuvers define their own `rudder()` function.
        """
        pass

    def evaluate(self):
        """
        Default dummy evaluate function.
        Specific maneuvers define their own `evaluate()` function.
        """
        pass

    def _nearest_neighbor(self, key, i, target):
        """
        Find the nearest neighbor.
        """

        diff_im1 = self.state.series[key][i-1] - target
        diff_i = self.state.series[key][i] - target
        diff_ip1 = self.state.series[key][i+1] - target

        if abs(diff_im1) - abs(diff_ip1) < 0:
            return i-1 if abs(diff_im1) < abs(diff_i) else i
        else:
            return i+1 if abs(diff_ip1) < abs(diff_i) else i

    def _interpol(self, key_y, i, key_x, x):
        """
        Interpolate linear
        """

        y2 = self.state.series[key_y][i]
        y1 = self.state.series[key_y][i-1]
        x2 = self.state.series[key_x][i]
        x1 = self.state.series[key_x][i-1]
        return (y2 - y1) / (x2 - x1) * (x - x1) + y1

    def _finished(self, tStep):
        """
        Check if the end condition of the maneuver is fulfilled.

        :param tStep:
            Current time step.

        :returns: True or False
        """

        if tStep >= self.tStep_max:
            return True

        return False

    def _printInfo(self):
        """
        Info to print before a maneuver is started.
        """

        s = self.ship.data
        sd = self.ship.data_dash
        p = self.parameter

        print("")
        print("+ ---------------------------------------")
        print("|   Parameter:                           ")
        print("+ ---------------------------------------")
        print("|")
        print("|   Ship:         {}".format(p['ship']))
        print("|   Maneuver:     {}".format(self.name))
        print("|   Rudder angle: {:.2f} [deg]".format(p['rudder_angle']))
        if p['maneuver'] == 'Zigzag':
            print("|   Course angle: {:.2f} [deg]".format(p['course_angle']))
        print("|")
        print("|   Timestep:     {:.3e} [-]".format(p['tstepsize']))
        print("|")
        print("+ ---------------------------------------")
        print("|  Ship data:                            ")
        print("+ ---------------------------------------")
        print("|")
        print("|  Length:       {: >14.3f} [m]".format(s['length']))
        print("|  Displacement: {: >14.3f} [t]".format(
            s['displacement'] / 1000.))
        print("|  Xg:           {: >14.3f} [m]".format(s['xg']))
        print("|  Izz:          {: >14.6E} [m^4]".format(s['izz']))
        print("|  U0:           {: >14.3f} [kn]".format(s['u0']))
        print("|  Delta rate:   {: >14.3f} [deg/s]".format(s['delta_rate']))
        print("|  Delta zero:   {: >14.3f} [deg]".format(s['delta_zero']))
        print("|")
        print("+ ---------------------------------------")
        print("|  Ship data (dimensionless):            ")
        print("+ ---------------------------------------")
        print("|")
        print("|  Length:       {: >14.6E} [-]".format(sd['length']))
        print("|  Displacement: {: >14.6E} [-]".format(sd['displacement']))
        print("|  Xg:           {: >14.6E} [-]".format(sd['xg']))
        print("|  Izz:          {: >14.6E} [-]".format(sd['izz']))
        print("|  U0:           {: >14.6E} [-]".format(sd['u0']))
        print("|  Delta rate:   {: >14.6E} [-]".format(sd['delta_rate']))
        print("|  Delta zero:   {: >14.6E} [-]".format(sd['delta_zero']))
        print("|")
        print("+ ---------------------------------------")
        print("")

    def run(self, stop=Event(), success=Event()):
        """
        Run the maneuver.

        :param stop:
            Event() flag received by this function to stop execution.

        :param success:
            Event() flag set by this function when maneuver was successfull.
        """

        logger.debug("Running maneuver '{}' with ship '{}'".format(
            self.name, self.ship.type))

        self._printInfo()

        print("")
        print("+ ---------------------------------------")
        print("|  Simulation:                           ")
        print("+ ---------------------------------------")
        print("|")

        start = time.time()
        tStep = 0
        with open(config.OFILE, 'w') as f:
            self.state.writeHeader(f)
            self.state.write(f)  # write state at beginning of maneuver
            while True:
                self.state = self.solver.nextStep(self.state)
                self.rudder()
                self.state.time += self.dt

                # if tStep % mod == 0:
                self.state.append()
                self.state.write(f)

                # make sure 'sign-of-live' is a multiple of first if
                if tStep % 25000 == 0:
                    print('|  Timestep: {:d}K/{:d}K, {:.2f} s'.format(
                        tStep//1000,
                        self.tStep_max//1000,
                        time.time() - start))

                if stop.is_set() or self._finished(tStep):
                    break

                tStep += 1

        if stop.is_set():
            msg = 'Simulation aborted by user:'
        else:
            msg = 'Simulation successfully finished:'
        print("|")
        print("+ ---------------------------------------")
        print("|")
        print("|  {}".format(msg))
        print("|  {:d}K/{:d}K timesteps in {:.2f} sec".format(
            tStep//1000,
            self.tStep_max//1000,
            time.time() - start))
        print("|")
        print("|  Simulation data written to:")
        print("|  {}".format(config.OFILE))
        print("|")
        print("+ ---------------------------------------")
        print("")

        success.set() if not stop.is_set() else success.clear()

        logger.info("Maneuver '{}' with ship '{}' finished".format(
            self.name, self.ship.type))

    @staticmethod
    def new(parameter):
        """
        Wrapper function to initialize an instance of a specific maneuver.

        :param parameter:
            Parameter for maneuver/computation.
        """

        if parameter['maneuver'] == TurningCircle.name:
            maneuver = TurningCircle(parameter)
        elif parameter['maneuver'] == Zigzag.name:
            maneuver = Zigzag(parameter)
        elif parameter['maneuver'] == SteadyAhead.name:
            maneuver = SteadyAhead(parameter)
        else:
            logger.error('Unknown Maneuver')
            maneuver = None
        return maneuver


class SteadyAhead(Maneuver):
    """
    Going forward with neutral rudder angle.

    Use :class:`Maneuver.new()` to initialize this class.

    :param parameter:
        Parameter for maneuver/computation.
    """

    name = 'Steady Ahead'

    def __init__(self, parameter):
        super().__init__(parameter)

    def evaluate(self):
        """
        Evaluate the *Steady Ahead* maneuver.

        :returns results:
            Results of the maneuver.
        """
        pass


class TurningCircle(Maneuver):
    """
    Simulate a Turning Circle maneuver.

    Use :class:`Maneuver.new()` to initialize this class.

    :param parameter:
        Parameter for maneuver/computation.
    """

    name = 'Turning Circle'

    def __init__(self, parameter):
        super().__init__(parameter)

        self.turningcircle_max = self.parameter['turningcircle_max'] \
            * numpy.pi/180.

        self.delta_max_sign = numpy.sign(self.delta_max)

    def rudder(self):
        """
        *Turning Circle* rudder function.

        :returns:
            Rudder angle for next time step.
        """

        delta = self.state.delta
        if abs(delta) < abs(self.delta_max):
            delta += self.dt * self.delta_rate * self.delta_max_sign
        else:
            delta = self.delta_max

        self.state.delta = delta

    def _finished(self, tStep):
        """
        Check end condition of *Turning Circle* maneuver.

        * Numer of timesteps and

        * Maximum course angle

        :returns: True or False
        """

        if tStep >= self.tStep_max or \
                abs(self.state.psi) > self.turningcircle_max:
            return True

        return False

    def evaluate(self):
        """
        Evaluate the *Turning Circle* maneuver.

        :returns results:
            Results of the maneuver.
        """

        series = self.state.series

        print("")
        print("+ ---------------------------------------")
        print("|  Evaluation of Turning Circle:         ")
        print("+ ---------------------------------------")
        print("|")

        if abs(series['psi'][-1]) < 360:
            print("|  Cannot evaluate!!!")
            print("|  Maximum course angle: {:.2f} [deg]".format(series['psi'][-1]))
            print("|  A full circle (360°) is required.")
            print("|")
            print("+ ---------------------------------------")
            print("")
            return

        i = next(j for j, psi in enumerate(
            series['psi']) if abs(psi) >= 90)
        i = self._nearest_neighbor('psi', i, numpy.sign(series['psi'][i])*90)
        psi90 = series['psi'][i]
        t90 = series['time'][i]
        x90 = series['x0'][i]
        y90 = series['y0'][i]

        i += next(j for j, psi in enumerate(
            series['psi'][i:]) if abs(psi) >= 180)
        i = self._nearest_neighbor('psi', i, numpy.sign(series['psi'][i])*180)
        psi180 = series['psi'][i]
        t180 = series['time'][i]
        x180 = series['x0'][i]
        y180 = series['y0'][i]

        i = next(j for j, x0 in enumerate(
            series['x0']) if series['x0'][j+1] < series['x0'][j])
        x0_max = series['x0'][i]
        x0_max_y = series['y0'][i]

        if self.delta_max > 0:  # y0 < 0
            i = next(j for j, y0 in enumerate(
                series['y0']) if series['y0'][j] < 0.)
            i += next(j for j, y0 in enumerate(
                series['y0'][i:]) if series['y0'][i+j+1] > series['y0'][i+j])
        else:  # y0 > 0
            i = next(j for j, y0 in enumerate(
                series['y0']) if series['y0'][j] > 0.)
            i += next(j for j, y0 in enumerate(
                series['y0'][i:]) if series['y0'][i+j+1] < series['y0'][i+j])
        y0_max = series['y0'][i]
        y0_max_x = series['x0'][i]
        # print(series['y0'][i-1], series['y0'][i], series['y0'][i+1])

        # steady state
        r = series['r'][-1]
        u = series['u'][-1]
        v = series['v'][-1]
        radius = numpy.sqrt(u * u + v * v)/(r/180.*numpy.pi)
        beta = numpy.arctan(-v/u)*180./numpy.pi

        # maneuver time
        t = series['time'][-1]

        u *= 3600./1852  # convert to knots

        print("|  Maneuver time:")
        print("|  {:.1f} sec, {:.1f} min".format(t, t/60))
        print("|")
        print("|")
        print("|  psi90:  {: >10.3f} [deg]".format(psi90))
        print("|  t90:    {: >10.3f} [s]".format(t90))
        print("|  x90:    {: >10.3f} [m]".format(x90))
        print("|  y90:    {: >10.3f} [m]".format(y90))
        print("|  psi180: {: >10.3f} [deg]".format(psi180))
        print("|  t180:   {: >10.3f} [s]".format(t180))
        print("|  y180:   {: >10.3f} [m]".format(y180))
        print("|")
        print("|  x0_max: {: >10.3f} [m]".format(x0_max))
        print("|  y0_max: {: >10.3f} [m]".format(y0_max))
        print("|")
        print("|  Steady state:")
        print("|  -------------")
        print("|  u:      {: >10.3f} [kn]".format(u))
        print("|  u/U0:   {: >10.3f} [-]".format(u/self.ship.data['u0']))
        print("|  R:      {: >10.3f} [m]".format(radius))
        print("|  r:      {: >10.3f} [deg/s]".format(r))
        print("|  beta:   {: >10.3f} [deg]".format(beta))
        print("|")
        print("+ ---------------------------------------")
        print("")

        # save results
        self.results['psi90'] = psi90
        self.results['t90'] = t90
        self.results['x90'] = x90
        self.results['y90'] = y90

        self.results['psi180'] = psi180
        self.results['t180'] = t180
        self.results['x180'] = x180
        self.results['y180'] = y180

        self.results['x0_max'] = x0_max
        self.results['x0_max_y'] = x0_max_y
        self.results['y0_max'] = y0_max
        self.results['y0_max_x'] = y0_max_x

        self.results['u'] = u
        self.results['R'] = abs(radius)
        self.results['r'] = r
        self.results['beta'] = beta
        self.results['time'] = t


class Zigzag(Maneuver):
    """
    Simulate a *Zigzag* maneuver.

    Use :class:`Maneuver.new()` to initialize this class.

    :param parameter:
        Parameter for maneuver/computation.
    """

    name = 'Zigzag'

    def __init__(self, parameter):
        super().__init__(parameter)

        self.zigzag_max = self.parameter['zigzag_max'] * 2 + 1

        self.zzstat = 0
        self.psi_prev = self.state.psi
        self.zigzag_n = 0

    def rudder(self):
        """
        *Zigzag* rudder function.

        :returns:
            Rudder angle for next time step.
        """
        psi = self.state.psi
        r = self.state.r
        delta = self.state.delta

        if self.delta_max > 0.:
            if self.zzstat == 0:
                if delta < self.delta_max:
                    delta += self.dt * self.delta_rate
                else:
                    delta = self.delta_max
                if abs(self.psi_max) <= -psi and r < 0:
                    self.zzstat = 1
                else:
                    self.zzstat = 0
            else:  # self.zzstat == 1
                if delta > -self.delta_max:
                    delta -= self.dt * self.delta_rate
                else:
                    delta = -self.delta_max
                if abs(self.psi_max) <= psi and r > 0:
                    self.zzstat = 0
                else:
                    self.zzstat = 1
        else:
            if self.zzstat == 0:
                if delta > self.delta_max:
                    delta += self.dt * -self.delta_rate
                else:
                    delta = self.delta_max
                if abs(self.psi_max) <= psi and r > 0:
                    self.zzstat = 1
                else:
                    self.zzstat = 0
            else:  # self.zzstat == 1
                if delta < -self.delta_max:
                    delta += self.dt * self.delta_rate
                else:
                    delta = -self.delta_max
                if abs(self.psi_max) <= -psi and r < 0:
                    self.zzstat = 0
                else:
                    self.zzstat = 1

        self.state.delta = delta

    def _finished(self, tStep):
        """
        Check end condition of *Zigzag* maneuver.

        * Numer of timesteps and

        * Number of Zigzags carried out

        :returns: True or False
        """

        if numpy.sign(self.state.psi) != numpy.sign(self.psi_prev):
            self.zigzag_n += 1

        self.psi_prev = self.state.psi

        if tStep >= self.tStep_max or \
                abs(self.zigzag_n) >= self.zigzag_max:
            return True

        return False

    def evaluate(self):
        """
        Evaluate the *Zigzag* maneuver.

        :returns results:
            Results of the maneuver.
        """

        series = self.state.series

        self.psi_max *= 180./numpy.pi

        print("+ ---------------------------------------")
        print("|  Evaluation of Zigzag:")
        print("+ ---------------------------------------")
        print("|")

        zigzags = 3
        if self.zigzag_n < zigzags + 1:
            print("|  Cannot evaluate!!!")
            print("|  Only {} zigzags reached.".format(self.zigzag_n))
            print("|  A minimum of {} is needed.".format(zigzags))
            print("|")
            print("+ ---------------------------------------")
            print("")
            return

        # Anschwenkzeit
        n = len(series['time'])

        iT = 0
        iTa = 0

        Ta = None
        for i in range(n-1):
            if abs(series['psi'][i]) <= abs(self.psi_max) and \
               abs(self.psi_max) <= abs(series['psi'][i+1]):
                i = self._nearest_neighbor('psi', i, numpy.sign(self.psi_max)*abs(self.psi_max))
                Ta = series['time'][i]
                iT = i
                iTa = i
                break

        # Stuetzzeit
        Tb = None
        for i in range(iT, n-1):
            if series['r'][i] < 0 and series['r'][i+1] > 0 or \
               series['r'][i] > 0 and series['r'][i+1] < 0:
                i = self._nearest_neighbor('r', i, 0.)
                Tb = series['time'][i] - Ta
                iT = i
                break

        # Ausweichzeit, Nulldurchgang psi
        Tt = None
        for i in range(iT, n-1):
            if series['psi'][i] < 0 and series['psi'][i+1] > 0 or \
               series['psi'][i] > 0 and series['psi'][i+1] < 0:
                i = self._nearest_neighbor('psi', i, 0.)
                Tt = series['time'][i]
                iT = i
                break

        # Rueckdrehzeit
        Tc = Tt - Ta - Tb

        # Periodendauer
        count = 0
        for i in range(iT+1, n-1):
            condition1 = series['psi'][i] < 0 and series['psi'][i+1] > 0
            condition2 = series['psi'][i] > 0 and series['psi'][i+1] < 0
            if (condition1 or condition2) and count == 0:
                count = 1
            elif (condition1 or condition2) and count != 0:
                i = self._nearest_neighbor('psi', i, 0.)
                T = series['time'][i] - Tt
                break

        A0 = [None]*zigzags
        count = 1
        for i in range(iTa, n-1):
            if count < zigzags + 1:
                if series['r'][i] < 0 and series['r'][i+1] > 0 or \
                   series['r'][i] > 0 and series['r'][i+1] < 0:
                    # Drehrate aendert Vorzeichen - Ueberschwingwinkel erreicht
                    i = self._nearest_neighbor('r', i, 0.)
                    A0[count-1] = numpy.sign(series['psi'][i]) \
                        * (abs(series['psi'][i]) - abs(self.psi_max))
                    count += 1
            else:
                break

        ymax = [None]*zigzags
        count = 0
        for i in range(iTa, n-1):
            # Transfer - Extrema
            if count < zigzags:
                if series['y0'][i] < series['y0'][i+1] > series['y0'][i+2] or \
                   series['y0'][i] > series['y0'][i+1] < series['y0'][i+2]:
                    ymax[count] = series['y0'][i+1]
                    count += 1
            else:
                break

        rmax = [None for i in range(zigzags)]
        count = 0
        for i in range(iTa, n-1):
            # Drehgeschwindigkeit - Extrema
            if count < zigzags:
                if series['rp'][i] < 0 and series['rp'][i+1] > 0 or \
                   series['rp'][i] > 0 and series['rp'][i+1] < 0:
                    i = self._nearest_neighbor('r', i, 0.)
                    rmax[count-1] = series['r'][i]
                    count += 1
            else:
                break

        t = series['time'][-1]

        print("|  Maneuver time:")
        print("|  {:.1f} sec, {:.1f} min".format(t, t/60))
        print("|")
        print("|")
        print("|  Anschwenkzeit: {: >10.3f} [s]".format(Ta))
        print("|  Stuetzzeit:    {: >10.3f} [s]".format(Tb))
        print("|  Rueckdrehzeit: {: >10.3f} [s]".format(Tc))
        print("|  Ausweichzeit:  {: >10.3f} [s]".format(Tt))
        print("|  Periode:       {: >10.3f} [s]".format(T))
        print("|")
        print("|  Ueberschwingwinkel:")
        for i in range(zigzags):
            print("|     A0{}:        {: >10.3f} [deg]".format(i+1, A0[i]))
        print("|  Transfer-Maxima:")
        for i in range(zigzags):
            print("|     Y0MAX{}:     {: >10.3f} [m]".format(i+1, ymax[i]))
        print("|  Drehrate-Maxima:")
        for i in range(zigzags):
            print("|     RMAX{}:      {: >10.3f} [deg/s]".format(i+1, rmax[i]))
        print("|")
        print("+ ---------------------------------------")
        print("")

        # save results
        self.results['Anschwenkzeit'] = Ta
        self.results['Stuetzzeit'] = Tb
        self.results['Rueckdrehzeit'] = Tc
        self.results['Ausweichzeit'] = Tt
        self.results['Periode'] = T

        self.results['Ueberschwingwinkel'] = A0
        self.results['Transfer-Maxima'] = ymax
        self.results['Drehrate-Maxima'] = rmax

        self.results['time'] = t
