"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

import argparse
import logging

import maneuverstudio.config as config

from maneuverstudio.maneuver import Maneuver
from maneuverstudio.gui.gui import gui

FORMAT = '[%(asctime)s] %(name)s %(levelname)s: %(message)s'
logger = logging.basicConfig(format=FORMAT, level='WARNING')
logger = logging.getLogger(__name__)


def parser():
    """
    sys.argv argument parser.
    """
    parser = argparse.ArgumentParser(
        prog='ManeuverStudio',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="""
Simulate nautical maneuvers:
The program is based on the approach of hydrodynamic coefficients to
compute the forces acting on the ship. A set of 46 coefficients is used.

Reference: https://doi.org/10.15480/882.841 (German)

Supported ships: {}
Supported maneuvers: {}
""".format(config.SHIP, config.MANEUVER),
        epilog="""
Documentation at {}

COPYRIGHT (c) 2018 Robert Beckmann, Raphael Bevand
Licensed under MIT license.
See the LICENSE file in the project root for license terms.
""".format(config.URL))

    parser.add_argument(
        '--no-gui',
        action='store_true',
        default=False,
        help='disable GUI, run the given maneuver headless')
    parser.add_argument(
        '--ofile',
        '-o',
        type=argparse.FileType('w'),
        default=config.OFILE,
        help='log file of simulation data (default: state.log). Pass None to disable logging.')
    parser.add_argument(
        '-v', '--verbose',
        action='count',
        default=0,
        help='verbose level. -v: Info -vv: Debug')

    parser.add_argument('ship', nargs='?', help='{}'.format(config.SHIP))
    parser.add_argument(
        'maneuver', nargs='?', help='{}'.format(config.MANEUVER))
    parser.add_argument(
        'rudder_angle',
        type=float,
        nargs='?',
        help='Angle of [-{0}, {0}] [deg]'.format(config.DELTAMAX))
    parser.add_argument(
        'course_angle',
        type=float,
        nargs='?',
        help='Angle of [0, {}] [deg]'.format(config.PSIMAX))

    return parser


def check_input(parameter):
    """
    Verify if the input uses only valid options
    within the valid limits.

    :param parameter:
        Parameter to verify.
    """
    p = parameter

    if p['maneuver'] not in config.MANEUVER:
        raise ValueError(
            'Unknown maneuver: {}. Available maneuvers: {}.'.format(
                p['maneuver'], config.MANEUVER))

    if p['ship'] not in config.SHIP:
        raise ValueError(
            'Unknown ship: {}. Available ships: {}'.format(
               p['ship'], config.SHIP))

    if config.DELTAMAX < abs(p['rudder_angle']):
        raise ValueError(
            'Rudder angle out of bounds! Maximum is +/-{} deg.'.format(
                config.DELTAMAX))

    if not config.PSIMAX >= p['course_angle'] >= 0.:
        raise ValueError(
            'Course angle out of bounds! [{}, {}] deg.'.format(
                   0., config.PSIMAX))

    if not config.TSTEPSIZEMAX >= p['tstepsize'] >= config.TSTEPSIZEMIN:
        raise ValueError(
            'Time step out of bounds! [{}, {}].'.format(
              config.TSTEPMIN, config.TSTEPMAX))

    tSteps = p['time_max'] / p['tstepsize']
    if not config.TSTEPMAX >= tSteps >= 0:
        raise ValueError(
            'Timestep number out of bounds! [{}, {}]'.format(
                   0., config.TSTEPMAX))


def headless(parameter):
    """
    Run the maneuver without GUI.

    :param parameter:
        Parameter for maneuver/computation.
    """

    maneuver = Maneuver.new(parameter)
    maneuver.run()
    maneuver.evaluate()


def main(*args, **kwargs):
    p = parser()
    args = p.parse_args()

    if args.verbose == 1:
        logging.getLogger().setLevel('INFO')
        logger.info("Logging has been set to 'INFO'")
    elif args.verbose == 2:
        logging.getLogger().setLevel('DEBUG')
        logger.info("Logging has been set to 'DEBUG'")

    # Load default parameter from config.
    parameter = config.PARAMETER

    if args.ship:
        parameter['ship'] = args.ship
    if args.maneuver:
        parameter['maneuver'] = args.maneuver
    if args.rudder_angle:
        parameter['rudder_angle'] = args.rudder_angle
    if args.course_angle:
        parameter['course_angle'] = args.course_angle

    check_input(parameter)

    if not args.no_gui:
        gui(parameter)
    else:
        headless(parameter)
