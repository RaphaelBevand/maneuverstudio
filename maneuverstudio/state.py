"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

from math import pi
import logging

logger = logging.getLogger(__name__)


class State:
    """
    Current state of the maneuver.

    Initialize the state before running a maneuver.

    The maneuver state is defined by dimensionless data:

    * time: `time`,

    * location: `x0, y0, psi`,

    * velocity: `u, v, r`,

    * acceleration: `up, vp, rp`,

    * forces: `X, Y, N` and

    * rudder angle: `delta`


    :param ship:
        Instance of ship class.
    """

    keys = [
        'time',
        'x0', 'y0', 'psi',
        'u', 'v', 'r',
        'up', 'vp', 'rp',
        'X', 'Y', 'N',
        'delta']

    _len = len(keys)

    def __init__(self, ship):

        L = ship.data['length']
        U = ship.data['u0'] * 1852./3600.
        rad2deg = 180./pi

        self.time = 0.
        self.x0 = 0.
        self.y0 = 0.
        self.psi = 0.
        self.u = ship.data_dash['u0']
        self.v = 0.
        self.r = 0.
        self.up = 0.
        self.vp = 0.
        self.rp = 0.
        self.X = 0.
        self.Y = 0.
        self.N = 0.
        self.delta = ship.data_dash['delta_zero']

        self._scale = [
            L/U,
            L,
            L,
            rad2deg,
            U,
            U,
            U/L * rad2deg,
            U*U/L,
            U*U/L,
            U*U/(L*L) * rad2deg,
            1.,
            1.,
            1.,
            rad2deg,
        ]

        # This dictionary holds the time series of the state vector.
        self.series = {k: [v] for k in State.keys for v in self._dim()}

        logger.debug("Initialized State of ship")

    def _dim(self):
        """
        :returns: The dimensionalized state vector.
        """
        return [self._scale[i] * v for i, v in enumerate((
            self.time,
            self.x0,
            self.y0,
            self.psi,
            self.u,
            self.v,
            self.r,
            self.up,
            self.vp,
            self.rp,
            self.X,
            self.Y,
            self.N,
            self.delta))]

    def append(self):
        """
        Append the current state *dimensionalized* to the time series.
        """

        # v = self._dim()
        # for i, k in enumerate(SimState.keys):
        #     self.series[k].append(v[i])
        # unroll... is faster...
        self.series['time'].append(self.time * self._scale[0])
        self.series['x0'].append(self.x0 * self._scale[1])
        self.series['y0'].append(self.y0 * self._scale[2])
        self.series['psi'].append(self.psi * self._scale[3])
        self.series['u'].append(self.u * self._scale[4])
        self.series['v'].append(self.v * self._scale[5])
        self.series['r'].append(self.r * self._scale[6])
        self.series['up'].append(self.up * self._scale[7])
        self.series['vp'].append(self.vp * self._scale[8])
        self.series['rp'].append(self.rp * self._scale[9])
        self.series['X'].append(self.X * self._scale[10])
        self.series['Y'].append(self.Y * self._scale[11])
        self.series['N'].append(self.N * self._scale[12])
        self.series['delta'].append(self.delta * self._scale[13])

    @staticmethod
    def writeHeader(fd):
        """
        Write header (State.keys) to file descriptor.

        :param fd:
            File Descriptor
        """
        fd.write("".join(("# ({}) {}\n".format(i, k) for i, k in enumerate(
            State.keys))))
        fd.write((" ".join(k for k in State.keys)) + "\n")

    def write(self, fd):
        """
        Write current state *dimensionalized*.

        :param fd:
            File Descriptor
        """

        s = (" ".join("{:.6E}".format(e) for e in self._dim()))
        fd.write(s + "\n")
        # s = (" ".join("{: .6E}".format(e) for e in [
        #     self.time * self._scale[0],
        #     self.x0 * self._scale[1],
        #     self.y0 * self._scale[2],
        #     self.psi * self._scale[3],
        #     self.u * self._scale[4],
        #     self.v * self._scale[5],
        #     self.r * self._scale[6],
        #     self.up * self._scale[7],
        #     self.vp * self._scale[8],
        #     self.rp * self._scale[9],
        #     self.X * self._scale[10],
        #     self.Y * self._scale[11],
        #     self.N * self._scale[12],
        #     self.delta * self._scale[13]]))
        # f.write(s + "\n")

    def __len__(self):
        """
        :returns: Number of state variables.
        """
        return self._len

    def __repr__(self):
        return (", ".join("{:.6f}".format(e) for e in [
            self.time,
            self.x0,
            self.y0,
            self.psi,
            self.u,
            self.v,
            self.r,
            self.up,
            self.vp,
            self.rp,
            self.X,
            self.Y,
            self.N,
            self.delta]))
