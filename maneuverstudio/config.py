"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

URL = 'https://orkusinum.gitlab.io/maneuverstudio/'
OFILE = 'state.log'


# ***** Constants

# density of water
rho = 1025.


# ***** Values and limits of simulation parameters.

# Available ships
SHIP = ('Container', 'Ferry', 'Series 60', 'Tanker')
# Available maneuvers
MANEUVER = ('Steady Ahead', 'Turning Circle', 'Zigzag')

# limits of simulation parameters

TSTEPMAX = 150000
TSTEPSIZEMIN = 1.e-4
TSTEPSIZEMAX = 1.   # [TSEPSIZEMIN, TSTEPSIZEMAX]
PSIMAX = 90.        # [0,, PSIMAX]
DELTAMAX = 40.      # [-DELTAMAX, DELTAMAX]


# ***** Default parameter

PARAMETER = {
    'maneuver': 'Steady Ahead',
    'ship': 'Container',
    'rudder_angle': 0.0,
    'course_angle': 0.0,

    # maximum in deg. 1080/360 = 3 circles
    'turningcircle_max': 1080,
    # maximum number of zigzags
    'zigzag_max': 5,

    # dimensionless parameters
    'tstepsize': 1e-3,
    'time_max': 150,

    'realtime': False
}
