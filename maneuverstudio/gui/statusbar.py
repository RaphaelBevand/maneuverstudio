"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

import tkinter as tk
import logging

from maneuverstudio.gui.simulation import SIM_STATUS

gui_logger = logging.getLogger(__name__)


class UIStatusbar:
    def __init__(self, master, sim_status):

        self.sim_status = sim_status

        frame = tk.Frame(
            master,
            relief=tk.SUNKEN,
            bd=1)
        frame.pack(
            side=tk.LEFT,
            fill=tk.X,
            expand=True)

        self.msg = tk.Label(
            frame,
            text="Please set parameters and start the simulation… ",
            padx=5)

        self.msg.pack(side=tk.LEFT)

        sim_status.trace("w", self.set_status)

    def set_status(self, *args):

        if self.sim_status.get() == SIM_STATUS.ACTIVE.value:

            self.msg.config(text="Simulation is running.")

        elif self.sim_status.get() == SIM_STATUS.STOPPED.value:

            self.msg.config(text="Simulation stopped.")

        elif self.sim_status.get() == SIM_STATUS.SUCCESS.value:

            self.msg.config(text="Simulation successfully finished.")
