"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

import tkinter as tk
import threading
import logging
import matplotlib
import numpy as np

from matplotlib.backends.backend_tkagg import \
                    FigureCanvasTkAgg, NavigationToolbar2Tk
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib.animation as animation
from matplotlib import style

from maneuverstudio.gui.util import ship, rot_ship

style.use('classic')
matplotlib.use(plt.get_backend())

gui_logger = logging.getLogger(__name__)


class UIOutput:
    def __init__(self, master, simulation):

        self.master = master
        self.simulation = simulation

        thread = threading.Thread(target=self.draw_XY)
        thread.daemon = True
        thread.start()

        gui_logger.debug('Loaded UIOutput')

        self.ship, self.rudder = ship(simulation.maneuver.ship)

    def draw_XY(self):

        self.frame = tk.Frame(self.master)
        self.frame.pack(fill=tk.BOTH, expand=True)
        self.fig = plt.Figure(figsize=(4, 1))
        gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
        self.canvas = FigureCanvasTkAgg(self.fig, master=self.frame)
        self.canvas.get_tk_widget().pack(fill=tk.BOTH, expand=True)
        self.toolbar = NavigationToolbar2Tk(self.canvas, self.frame)
        self.toolbar.update()
        self.canvas._tkcanvas.pack()

        self.fig.subplots_adjust(
            left=0.075,
            right=0.975,
            top=0.95)

        self.plot_xy = self.fig.add_subplot(gs[0])
        self.plot_xy.axis('equal')
        self.plot_xy.minorticks_on()
        self.plot_xy.grid(which='major', ls=':', lw='0.25', color='black')
        self.plot_xy.grid(which='minor', ls=':', lw='0.25', color='black')
        self.plot_xy.set_xlabel('Distance X0 [m]')
        self.plot_xy.set_ylabel('Distance Y0 [m]')
        self.plot_xy.set_ylim([-7500, 7500])
        #self.plot_xy.set_xlim([-5000, 10000])
        self.plot_xy.set_xlim([-500, 10000])

        x = [0, 0]
        y = [0, 0]
        self.ship_path, = self.plot_xy.plot(x, y, 'b-', lw=0.75)
        self.ship_cog, = self.plot_xy.plot(x, y, 'go', markersize=6)
        ship = rot_ship(self.ship, self.rudder, x[-1], y[-1], 0., 0.)
        self.ship_outline, = self.plot_xy.plot(ship[0], ship[1], 'k', lw=1)

        # self.plot_angles = self.fig.add_subplot(gs[1])
        # self.plot_angles.minorticks_on()
        # self.plot_angles.grid(which='major', ls=':', lw='0.25', color='black')
        # #self.plot_angles.grid(which='minor', ls=':', lw='0.25', color='black')
        # self.plot_angles.set_xlabel('Time [s]')
        # self.plot_angles.set_ylabel('Delta [deg.]')
        # self.plot_angles.set_ylim([-40, 40])
        # self.plot_angles.set_xlim([0, 1200])

        # self.delta, = self.plot_angles.plot(0, 0, 'b-', lw=0.75)
        # self.psi, = self.plot_angles.plot(0, 0, 'g-', lw=0.75)

        def animate(i):
            x = self.simulation.maneuver.state.series['x0'][:]
            y = self.simulation.maneuver.state.series['y0'][:]

            self.ship_path.set_data(x, y)

            self.ship_cog.set_data(x[-1], y[-1])

            psi = self.simulation.maneuver.state.series['psi'][-1] * np.pi/180.
            delta = self.simulation.maneuver.state.series['delta'][-1] * np.pi/180.

            ship = rot_ship(self.ship, self.rudder, x[-1], y[-1], psi, delta)
            self.ship_outline.set_data(ship[0], ship[1])

            # self.delta.set_data(
            #     self.simulation.maneuver.state.series['time'],
            #     self.simulation.maneuver.state.series['delta'])
            # self.psi.set_data(
            #      self.simulation.maneuver.state.series['time'],
            #      self.simulation.maneuver.state.series['psi'])

            return (
                self.ship_path,
                self.ship_cog,
                self.ship_outline)
                # self.delta,
                # self.psi)

        anim = animation.FuncAnimation(
            self.fig,
            animate,
            interval=1000//10,
            repeat=True,
            blit=True)
