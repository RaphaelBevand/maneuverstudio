"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

import numpy as np


def ship(ship):
    """
    Build a simple block shaped ship.

    :param ship:
        Object of class Ship.

    :returns:
        ship: Block shaped ship.
        rudder: Simple line for rudder.
    """

    L = ship.data['length']
    B = ship.data['breadth']
    XG = ship.data['xg']

    # simple ship
    # first and last point is aft at midship (rudder attaches here)
    ship = np.asarray([[
        -0.5 * L, -0.5 * L, 0.5 * L - 0.25 * L, 0.5 * L, 0.5 * L - 0.25 * L,
        -0.5 * L, -0.5 * L],
        [0., 0.5 * B, 0.5 * B, 0., -0.5 * B, -0.5 * B, 0.]])

    # adjust ship coordinates by cog of ship (xg)
    ship += [[XG], [0.]]

    # end point of rudder
    rudder = np.asarray([[-0.4 * B], [0.]])

    return (ship, rudder)


def rot_ship(ship, rudder, x, y, psi, delta):
    """
    Change rotation of ship and rudder.
    Move ship to target location.

    :param ship:
    Block shaped ship returned by `ship()`.

    :param rudder:
    Simple rudder returned by `ship()`.

    :param x, y:
    Global coordinates (Position of ship).

    :param psi:
    Course angle.

    :param delta:
    Rudder angle.

    :returns:
    Ship with attached rudder in global coordinates.
    """
    # rotation matrices
    rot_psi = np.asarray([
        [np.cos(psi), -np.sin(psi)],
        [np.sin(psi), np.cos(psi)]])

    rot_delta = np.asarray([
        [np.cos(delta), -np.sin(delta)],
        [np.sin(delta),  np.cos(delta)]])

    # rotate rudder by delta
    rudder = rot_delta @ rudder
    # attach the rudder to the ship
    ship = np.c_[ship, np.add(rudder, [[ship[0][0]], [ship[1][0]]])]
    # rotate ship by psi
    ship = rot_psi @ ship
    # move ship to target location
    ship = np.add(ship, [[x], [y]])

    return ship

