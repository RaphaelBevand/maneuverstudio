"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

import tkinter as tk
import logging
import threading
from enum import Enum

from maneuverstudio.maneuver import Maneuver

gui_logger = logging.getLogger(__name__)


class SIM_STATUS(Enum):
    NOTSET = 0
    ACTIVE = 1
    SUCCESS = 2
    STOPPED = 3


class Simulation:
    """
    Threaded maneuver simulation.

    Threading events initialized by this class:

        * `event_active`: Set when simulation is running,
                          cleared when simulation stopped or finished.

        * `event_success`: Evaluated after computation,
                           set by the Maneuver instance (`Maneuver.run()`)

        * `event_stop`: Evaluated by Maneuver instance (`Maneuver.run()`),
                        Set by user via GUI to stop the simulation.

    :param parameter:
        Parameter for maneuver/computation.
    """
    def __init__(self, parameter):
        gui_logger.debug('Initialize Simulation')
        self.parameter = parameter
        self.maneuver = Maneuver.new(self.parameter)

        self.event_active = threading.Event()
        self.event_success = threading.Event()
        self.event_stop = threading.Event()

        self.status = tk.IntVar()
        self.status.set(SIM_STATUS.NOTSET.value)

    def start(self):
        """
        Start Simulation.
        """
        if not self.event_active.is_set():
            self.event_stop.clear()
            self.event_success.clear()
            self.event_active.set()
            self.status.set(SIM_STATUS.ACTIVE.value)
            self.thread = threading.Thread(target=self._run)
            self.thread.daemon = True
            self.thread.start()
            gui_logger.info("Started simulation")
        else:
            gui_logger.info("Simulation is already running")

    def stop(self):
        """
        Stop Simulation.
        """
        self.event_stop.set()
        if self.event_active.is_set():
            self.event_active.clear()
            self.thread.join()
            self.status.set(SIM_STATUS.STOPPED.value)
            gui_logger.info("Stopped simulation")
        else:
            gui_logger.info("Simulation is not running")

    def _run(self):
        """
        Use start() to run a simulation.
        """
        self.maneuver = Maneuver.new(self.parameter)
        self.maneuver.run(self.event_stop, self.event_success)
        if self.event_success.is_set():
            self.status.set(SIM_STATUS.SUCCESS.value)
            self.maneuver.evaluate()
