"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

import tkinter as tk
from tkinter.ttk import Separator
import logging

import maneuverstudio.config as config

gui_logger = logging.getLogger(__name__)


class UIControl:
    """
    UIControl contains the the elements to control the simulation.
    Each element is placed in its own tab.
    """
    def __init__(self, master, simulation):

        gui_logger.debug('Loading UIControl...')

        self.master = master

        UIControl_StartStop(master, simulation)
        UIControl_Parameter(master, simulation)

        gui_logger.debug('Loaded UIControl')


class UIControl_StartStop:
    def __init__(self, master, simulation):

        self.simulation = simulation

        frame = tk.LabelFrame(master, text=' Simulation ', padx=5, pady=5)
        frame.pack(side=tk.TOP, fill=tk.X, padx=10, pady=5)

        self.button_start = tk.Button(
            frame,
            text="Start",
            command=self.start)
        self.button_start.pack(side=tk.LEFT, padx=20, fill=tk.X, expand=True)

        self.button_stop = tk.Button(
            frame,
            text="Stop",
            state=tk.DISABLED,
            command=self.stop)
        self.button_stop.pack(side=tk.LEFT, padx=20, fill=tk.X, expand=True)

        gui_logger.debug('Loaded UIControl_StartStop')

    def start(self):
        self.simulation.start()
        self.button_stop.config(state=tk.NORMAL)
        self.button_start.config(state=tk.DISABLED)

    def stop(self):
        self.simulation.stop()
        self.button_stop.config(state=tk.DISABLED)
        self.button_start.config(state=tk.NORMAL)


class UIControl_Parameter:
    """

    UIParams Layout:

    +----------------+
    |    frame       |
    |+--------------+|
    ||              ||
    ||  dialog-     ||
    ||      Frame   ||
    ||              ||
    |+--------------+|
    || buttonFrame  ||
    |+--------------+|
    || messageFrame ||
    |+--------------+|
    +----------------+

    """
    def __init__(self, master, simulation):

        self.simulation = simulation

        #
        # Frames
        #

        # main frame
        frame = tk.LabelFrame(master, text="Parameter", padx=5, pady=5)
        frame.pack(side=tk.TOP, fill=tk.X, padx=10, pady=5)

        # frame containing the parameter to change
        dialogFrame = tk.Frame(
            frame, borderwidth=1, padx=5, pady=5)
        dialogFrame.pack(side=tk.TOP, fill=tk.X)

        Separator(frame, orient=tk.HORIZONTAL).pack(side=tk.TOP, fill=tk.X)

        # frame containing the buttons to update/reset the chosen options
        buttonFrame = tk.Frame(
            frame, borderwidth=1, padx=5, pady=5)
        buttonFrame.pack()

        # frame containing the "accept" message
        self.buttonMessageFrame = tk.Frame(
            frame, borderwidth=1, padx=5, pady=5, width=200, height=25)
        # fixed size to make sure the frame does not grow uncontrolled
        self.buttonMessageFrame.pack_propagate(0)
        self.buttonMessageFrame.pack()

        #
        # Buttons inside the buttonFrame
        #

        r = 0
        self.button_accept = tk.Button(
            buttonFrame,
            text='Accept', command=self._accept, state=tk.DISABLED)
        self.button_accept.grid(
            row=r, column=0, columnspan=1, sticky=tk.W, padx=5)

        self.button_revert = tk.Button(
            buttonFrame,
            text='Revert', command=self._revert, state=tk.DISABLED)
        self.button_revert.grid(
            row=r, column=1, columnspan=1, sticky=tk.W, padx=5)

        #
        # Initialize the variables for the options
        #

        self.tstepsize = tk.StringVar()  # string for input checking, see _accept()
        self.ship = tk.StringVar()
        self.maneuver = tk.StringVar()
        self.rudderangle = tk.DoubleVar()
        self.courseangle = tk.DoubleVar()
        self._setValues()

        r = 0

        #
        # Initialize the elements inside the dialogFrame
        #

        self.label_tstepsize = tk.Label(
            dialogFrame, text='Timestepsize: ').grid(
            row=r, column=0, sticky=tk.W, padx=2)
        self.entry_tstepsize = tk.Entry(
            dialogFrame,
            textvariable=self.tstepsize).grid(
            row=r, column=1, columnspan=2, sticky='ew')
        self.tstepsize.trace("w", self._activateButtons)
        r += 1

        self.label_ship = tk.Label(
            dialogFrame, text='Ship: ').grid(
            row=r, column=0, sticky=tk.W, padx=2)
        self.parameter_ship = tk.OptionMenu(
            dialogFrame, self.ship, *config.SHIP).grid(
            row=r, column=1, columnspan=2, sticky='ew')
        self.ship.trace("w", self._activateButtons)
        r += 1

        self.label_maneuver = tk.Label(
            dialogFrame, text='Maneuver: ').grid(
            row=r, column=0, sticky=tk.W, padx=2)
        self.parameter_maneuver = tk.OptionMenu(
            dialogFrame, self.maneuver, *config.MANEUVER).grid(
            row=r, column=1, columnspan=2, sticky='ew')
        self.maneuver.trace("w", self._maneuverChanged)
        r += 1

        self.label_rudderangle = tk.Label(
            dialogFrame, text='Rudder angle (δ): ').grid(
            row=r, column=0, sticky=tk.W, padx=2)
        self.parameter_rudderangle = tk.Scale(
            dialogFrame,
            variable=self.rudderangle,
            fro=-30,
            to=30,
            resolution=2.5,
            digits=3,
            orient=tk.HORIZONTAL)
        self.parameter_rudderangle.grid(
            row=r, column=2, sticky=tk.W)
        self.rudderangle.trace("w", self._activateButtons)
        r += 1

        self.label_courseangle = tk.Label(
            dialogFrame, text='Course angle (Ψ): ').grid(
            row=r, column=0, sticky=tk.W, padx=2)
        self.parameter_courseangle = tk.Scale(
            dialogFrame,
            variable=self.courseangle,
            fro=0,
            to=45,
            resolution=2.5,
            digits=3,
            orient=tk.HORIZONTAL)
        self.parameter_courseangle.grid(
            row=r, column=2, sticky=tk.W)
        self.courseangle.trace("w", self._activateButtons)

        self._maneuverChanged()  # call once to define the slider state

        for row in range(0, r):
            dialogFrame.grid_rowconfigure(row, minsize=35)

        gui_logger.debug('Loaded UIControl_Parameter')

    def _setValues(self, revert=False):
        self.tstepsize.set(self.simulation.parameter['tstepsize'])
        self.ship.set(self.simulation.parameter['ship'])
        self.maneuver.set(self.simulation.parameter['maneuver'])
        self.rudderangle.set(self.simulation.parameter['rudder_angle'])
        self.courseangle.set(self.simulation.parameter['course_angle'])
        if not revert:
            gui_logger.debug('Initialized parameter from Simulation')

    def _revert(self):
        self._setValues(revert=True)
        gui_logger.debug('Reverted parameter')
        self._disableButtons()

    def _accept(self):

        try:
            float(self.tstepsize.get())
        except (ValueError):
            tk.messagebox.showerror(
                "Error",
                "Invalid timestep!\n" +
                "Please insert a number.")

        self.tstepsize.set(float(self.tstepsize.get()))
        #  ^ update the GUI: E.g. input of 0.00001 becomes 1e-5.
        parameter_new = self.simulation.parameter.copy()
        parameter_new['ship'] = self.ship.get()
        parameter_new['maneuver'] = self.maneuver.get()
        parameter_new['rudder_angle'] = self.rudderangle.get()
        parameter_new['course_angle'] = self.courseangle.get()
        parameter_new['tstepsize'] = float(self.tstepsize.get())

        # check all elements where users can enter random data
        # elements affected: tstepsize

        if not config.TSTEPSIZEMAX >= \
                parameter_new['tstepsize'] \
                >= config.TSTEPSIZEMIN:
            gui_logger.warning(
                "Chosen timestep is out of bounds: [{:.1E}, {}]".format(
                    config.TSTEPSIZEMIN, config.TSTEPSIZEMAX))
            tk.messagebox.showerror(
                "Error",
                "Invalid timestep!\n" +
                "Chosen timestep is out of bounds: [{:.1E}, {}]".format(
                    config.TSTEPSIZEMIN, config.TSTEPSIZEMAX))
            return

        self.simulation.parameter = parameter_new.copy()
        gui_logger.debug('Updated parameter:')
        gui_logger.debug(self.simulation.parameter)

        msg = tk.Label(
            self.buttonMessageFrame, text='Updated parameter.')
        msg.pack()
        msg.after(3000, msg.destroy)

        self._disableButtons()

    def _activateButtons(self, *args):
        self.button_revert.config(state=tk.NORMAL)
        self.button_accept.config(state=tk.NORMAL)

    def _disableButtons(self, *args):
        self.button_revert.config(state=tk.DISABLED)
        self.button_accept.config(state=tk.DISABLED)

    def _maneuverChanged(self, *args):

        # self.courseangle.set(0.0)
        # self.rudderangle.set(0.0)

        if self.maneuver.get() == 'Steady Ahead':

            self.parameter_courseangle.config(
                state='disabled', sliderrelief=tk.FLAT)
            self.parameter_rudderangle.config(
                state='disabled', sliderrelief=tk.FLAT)

        elif self.maneuver.get() == 'Turning Circle':

            self.parameter_courseangle.config(
                state='disabled', sliderrelief=tk.FLAT)
            self.parameter_rudderangle.config(
                state='normal', sliderrelief=tk.RAISED)

        else:
            self.parameter_courseangle.config(
                state='normal', sliderrelief=tk.RAISED)
            self.parameter_rudderangle.config(
                state='normal', sliderrelief=tk.RAISED)

        self._activateButtons()
