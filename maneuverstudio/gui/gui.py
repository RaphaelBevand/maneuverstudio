"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

import tkinter as tk
import logging
import webbrowser

import maneuverstudio.config as config

from maneuverstudio.gui.statusbar import UIStatusbar
from maneuverstudio.gui.control import UIControl
from maneuverstudio.gui.output import UIOutput
from maneuverstudio.gui.simulation import Simulation

gui_logger = logging.getLogger(__name__)


class GUI:
    """
    GUI of ManeuverStudio

    GUI main layout:

    +---------------------------------------+
    |                MenuBar                |
    +--------------+------------------------+
    |              |                        |
    |              |                        |
    |              |                        |
    |   control-   |       right-           |
    |     Frame    |         Frame          |
    |              |                        |
    |              |                        |
    |              |                        |
    |              |                        |
    +--------------+------------------------+


    +---------------------------------------+
    |                MenuBar                |
    +--------------+------------------------+
    |             |                         |
    |             |                         |
    |   control-  |       output-           |
    |    Frame    |           Frame         |
    |             |      (Output)           |
    |             |                         |
    |             |                         |
    |             +-------------------------+
    |             |       StatusBar         |
    +-------------+-------------------------+

    """

    def __init__(self, master, simulation):

        gui_logger.debug('Loading Graphical User Interface...')

        self.master = master
        self.simulation = simulation

        master.minsize(800, 500)
        master.geometry('1800x800')
        master.title("Maneuverstudio")

        self.controlFrame = tk.Frame(
            master, pady=10, padx=5, borderwidth=1, relief=tk.GROOVE)

        self.controlFrame_open = True  # Flag is used to toggle window.
        self.controlFrame.pack(side=tk.LEFT, fill=tk.Y)
        UIControl(self.controlFrame, self.simulation)

        rightFrame = tk.Frame(
            master, pady=10, padx=5, borderwidth=1, relief=tk.GROOVE)
        rightFrame.pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)
        outputFrame = tk.Frame(
            rightFrame, pady=5, padx=5)
        outputFrame.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        # add output
        UIOutput(outputFrame, simulation)

        # add status bar
        UIStatusbar(rightFrame, simulation.status)

        # *** Menu *** ###

        menubar = tk.Menu(master)

        helpmenu = tk.Menu(menubar, tearoff=0)
        helpmenu.add_command(label="Manual", command=self.menu_open_manual)
        helpmenu.add_command(label="About", command=None)

        menubar.add_command(label="Control", command=self.menu_control)
        menubar.add_cascade(label="Help", menu=helpmenu)
        menubar.add_command(label="Quit", command=self.menu_quit)

        # add menu
        master.config(menu=menubar)

        gui_logger.info('Loaded Graphical User Interface')

    def menu_open_manual(self):
        webbrowser.open_new_tab(config.URL)

    def menu_control(self):
        if self.controlFrame_open:
            self.controlFrame.pack_forget()
            self.controlFrame_open = False
        else:
            self.controlFrame.pack(side=tk.LEFT, fill=tk.Y)
            self.controlFrame_open = True

    def menu_quit(self):
        self.master.quit()


def gui(parameter):
    """
    Load the GUI.

    :param parameter:
        Parameter for maneuver/computation.
    """

    root = tk.Tk()

    # Initialize the simulation
    simulation = Simulation(parameter)

    GUI(root, simulation)
    root.mainloop()
