"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

from math import pi
import csv
import logging
import os

from maneuverstudio.config import rho

logger = logging.getLogger(__name__)


class Ship:
    """
    Ship data.

    * Load ship data.

    * Compute dimensionless data.

    * Load hydrodynamic coefficients.

    :param ship:
        Type of ship.
    """

    def __init__(self, ship):
        self.load(ship)

    def load(self, ship):

        self.type = ship

        logger.debug("Loading ship '{}'...".format(self.type))

        file = os.path.join(
            os.path.dirname(__file__), 'data/ship.csv')
        self.data = self._loadDataCSV(file)
        self._dimensionless()
        self._loadCoeff()

        logger.debug("Loaded ship '{}'".format(self.type))

    def _dimensionless(self):
        """
        Compute dimensionless data.
        """

        self.data_dash = self.data.copy()
        L = self.data['length']
        u0 = self.data['u0'] * 1852./3600.  # [m/s]
        d2rad = pi/180.

        self.data_dash['length'] = 1.
        self.data_dash['breadth'] /= L
        self.data_dash['displacement'] /= 0.5*rho*L**3
        self.data_dash['xg'] /= L
        self.data_dash['izz'] /= 0.5*rho*L**5
        self.data_dash['u0'] = 1.
        self.data_dash['delta_rate'] *= d2rad*L/u0
        self.data_dash['delta_zero'] *= d2rad
        # self.ship_dash['block_coeff']
        # self.ship_dash['lambda']

        logger.debug("Computed dimensionless data")

    def _loadCoeff(self):
        """
        Load hydrodynamic coefficients from CSV file.

        :param ship:
            Type of ship. E.g. Container
        """

        file = os.path.join(
            os.path.dirname(__file__), 'data/hydroCoeff.csv')
        self.coeff = self._loadDataCSV(file)

        logger.debug("Loaded hydrodynamic coefficients")

    def _loadDataCSV(self, file):
        """
        Load ship data from CSV file.

        :param file:
            Path to CSV file.
        """

        with open(file, 'r') as f:
            reader = csv.DictReader(f)
            data = {k: [] for k in reader.fieldnames if k != 'ship'}
            for row in reader:
                if row['ship'] == self.type:
                    try:
                        for k in data:
                            data[k].append(float(row[k]))
                    except ValueError:
                        continue
        for k in data:
            data[k] = data[k][0] if len(data[k]) == 1 else data[k]

        return data
