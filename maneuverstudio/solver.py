"""
Copyright (c) 2018-2019 Robert Beckmann, Raphael Bevand
Distributed under MIT License, see accompanying LICENSE file.

If a copy of the MIT License was not distributed with this file,
You can ontain one at https://opensource.org/licenses/MIT.
"""

import numpy
import logging

logger = logging.getLogger(__name__)


class Solver():
    """
    Solve the linear equation of motion.

    Reference: https://doi.org/10.15480/882.841 - Page 20.

    A set of 46 coefficients is used.

    :param ship:
        Instance of class ship.

    :param dt:
        Timestep size.
    """

    def __init__(self, ship, dt):

        self.dt = dt

        self.mass = ship.data_dash['displacement']
        self.izz = ship.data_dash['izz']
        self.xg = ship.data_dash['xg']
        self.u0 = ship.data_dash['u0']

        self.cx = numpy.asarray(ship.coeff['cx'])*1e-6
        self.cy = numpy.asarray(ship.coeff['cy'])*1e-6
        self.cn = numpy.asarray(ship.coeff['cn'])*1e-6

        logger.debug("Initialized Solver")

    def nextStep(self, state):
        """
        Compute the next state based on the old state.

        :param state:
            Current state of ship.

        :returns:
            New state of the ship.
        """

        u = state.u
        v = state.v
        r = state.r
        d = state.delta
        du = u - self.u0

        # ----
        # Avoid repetitive computation of the same expression
        # Avoid expensive 'power of' computation

        UPOW2 = u * u
        ONEOVERU = 1./u
        ONEOVERUPOW2 = ONEOVERU * ONEOVERU

        VPOW2 = v * v
        VPOW3 = VPOW2 * v

        RPOW2 = r * r
        RPOW3 = RPOW2 * r

        DUPOW2 = du * du
        DUPOW3 = UPOW2 * du

        DPOW2 = d*d
        DPOW3 = DPOW2 * d
        DPOW4 = DPOW3 * d
        DPOW5 = DPOW4 * d

        fMul = numpy.asarray([
            UPOW2, du*u, DUPOW2, DUPOW3*ONEOVERU, 0., 0.,
            v*u, VPOW2, VPOW3*ONEOVERU, v*abs(v), 0., 0.,
            r*u, RPOW2, RPOW3*ONEOVERU, r*abs(r), 0., 0.,
            d*UPOW2, DPOW2*UPOW2, DPOW3*UPOW2,
            DPOW4*UPOW2, DPOW5*UPOW2, d*abs(d)*UPOW2,
            v*du, v*DUPOW2*ONEOVERU, VPOW2*du*ONEOVERU, VPOW3*du*ONEOVERUPOW2,
            r*du, r*DUPOW2*ONEOVERU, RPOW2*du*ONEOVERU, RPOW3*du*ONEOVERUPOW2,
            d*du*u, d*DUPOW2, DPOW2*du*u, DPOW3*du*u,
            v*r, v*RPOW2*ONEOVERU, VPOW2*r*ONEOVERU, VPOW3*r*ONEOVERUPOW2,
            v*d*u, v*DPOW2*u, VPOW2*d,
            r*d*u, r*DPOW2*u, RPOW2*d])

        F = [
            numpy.dot(fMul, self.cx),
            numpy.dot(fMul, self.cy),
            numpy.dot(fMul, self.cn)]

        # ***** Solve linear system using Cramer's Rule *****

        a11 = self.mass - self.cy[10] \
            - self.cy[11] * VPOW2 * ONEOVERUPOW2

        a12 = self.mass*self.xg - self.cy[16] \
            - self.cy[17] * RPOW2 * ONEOVERUPOW2

        a21 = self.mass*self.xg - self.cn[10] \
            - self.cn[11] * VPOW2 * ONEOVERUPOW2

        a22 = self.izz - self.cn[16] \
            - self.cn[17] * RPOW2 * ONEOVERUPOW2

        b1 = -self.mass*u*r + F[1]
        b2 = -self.mass*self.xg*u*r + F[2]

        detA = a11 * a22 - a21 * a12
        detA1 = b1 * a22 - b2 * a12
        detA2 = a11 * b2 - a21 * b1

        state.rp = detA2 / detA
        state.vp = detA1 / detA
        state.up = (F[0] + self.mass * v * r + self.mass * self.xg * RPOW2) \
            / (self.mass - self.cx[4] - self.cx[5] * DUPOW2 * ONEOVERUPOW2)

        # ***** Integrate Euler 1. Order *****

        sin = numpy.sin(state.psi)
        cos = numpy.cos(state.psi)

        state.x0 += self.dt * (u*cos - v*sin)
        state.y0 += self.dt * (u*sin + v*cos)
        state.psi += self.dt * r

        state.u += self.dt * state.up
        state.v += self.dt * state.vp
        state.r += self.dt * state.rp

        state.X = F[0]
        state.Y = F[1]
        state.N = F[2]

        return state

        # # ***** Solve linear system using Cramer's Rule *****

        # a11 = self.mass - self.cy[10] - self.cy[11] * \
        #     VPOW2 * ONEOVERUPOW2

        # a12 = self.mass*self.xg - self.cy[16] - self.cy[17] * \
        #     RPOW2 * ONEOVERUPOW2

        # a21 = self.mass*self.xg - self.cn[10] - self.cn[11] * \
        #     VPOW2 * ONEOVERUPOW2

        # a22 = self.izz - self.cn[16] - self.cn[17] * \
        #     RPOW2 * ONEOVERUPOW2

        # b1 = -self.mass*u*r + F[1]
        # b2 = -self.mass*self.xg*u*r + F[2]

        # detA = a11 * a22 - a21 * a12
        # detA1 = b1 * a22 - b2 * a12
        # detA2 = a11 * b2 - a21 * b1

        # rp = detA2 / detA
        # vp = detA1 / detA
        # up = \
        #     (F[0] + self.mass * v * r + self.mass * self.xg * RPOW2) / \
        #     (self.mass - self.cx[4] - self.cx[5] * DUPOW2 * ONEOVERUPOW2)

        # # ***** Integrate Euler 1. Order *****

        # sin = numpy.sin(state.psi)
        # cos = numpy.cos(state.psi)

        # state.x0 += self.dt * (u*cos - v*sin)
        # state.y0 += self.dt * (u*sin + v*cos)
        # state.psi += self.dt * r

        # u += self.dt * up
        # v += self.dt * vp
        # r += self.dt * rp

        # # ***** Total Force *****
        # # "terms that were set to zero in fMul"

        # du = u - self.u0
        # ONEOVERUPOW2 = 1./(u * u)
        # VPOW2OVERUPOW2 = v * v * ONEOVERUPOW2
        # RPOW2OVERUPOW2 = r * r * ONEOVERUPOW2

        # state.X = F[0] + self.cx[4] * up + \
        #     self.cx[5] * up * du * du * ONEOVERUPOW2

        # state.Y = F[1] + \
        #     self.cy[10] * vp + \
        #     self.cy[11] * vp * VPOW2OVERUPOW2 + \
        #     self.cy[16] * rp - \
        #     self.cy[17] * rp * RPOW2OVERUPOW2

        # state.N = F[2] + \
        #     self.cn[10] * vp + \
        #     self.cn[11] * vp * VPOW2OVERUPOW2 + \
        #     self.cn[16] * rp - \
        #     self.cn[17] * rp * RPOW2OVERUPOW2

        # # ***** Update State Variable *****
        # # (x0, y0, psi are already updated)

        # state.rp = rp
        # state.vp = vp
        # state.up = up
        # state.r = r
        # state.v = v
        # state.u = u

        # return state
