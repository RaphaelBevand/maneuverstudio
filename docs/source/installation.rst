Installation
============

Requirements
------------

* Python 3

**Packages:** See *requirements.txt*.

Install
-------

`pipenv`
^^^^^^^^

.. code-block:: bash

   pip3 install pipenv

   git clone git@gitlab.com:orkusinum/maneuverstudio.git
   cd maneuverstudio/

   pipenv install
    pipenv shell

   # leave and cleanup
   exit
   pipenv --rm


`virtualenv`
^^^^^^^^^^^^

.. code-block:: bash

   virtualenv path/to/env
   source path/to/env/bin/activate

   git clone git@gitlab.com:orkusinum/maneuverstudio.git
   cd maneuverstudio/
   pip install -r requirements.txt

   # leave and cleanup
   deactivate
   rm -rf path/to/env

------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`