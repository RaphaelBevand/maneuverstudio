.. Maneuverstudio documentation master file, created by
   sphinx-quickstart on Mon Nov 26 19:40:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Maneuverstudio's documentation!
===========================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   installation

Indices and tables
^^^^^^^^^^^^^^^^^^

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
