function hydrodynLoads = a_hydrodynLoads(x, u0, delta, hydroCoeff, wolff)
% Function to calculate the hydrodynamic forces of a ship in the standard
% way with the option to apply the corrections of Wolff.
%
%   Function reads in the following arguments:
%      - state vector: state = [x0, y0, psi, u, v, r]
%      - initial ship velocity: u0
%      - rudder angle: delta
%      - hydrodynamic coefficients: coeff = [CX, CY, CN]
%      - Option to apply Wolff's correction: wolff = true/false
%
%   Function returns the hydrodynamic forces and the yaw moment, which
%   are NOT dependent on the ship's acceleration:
%      - hydrodynamic loads: hydroLoads = [X, Y, N]

% -------------------------------------------------------------------------
% Pass specific arguments to local variables
% -------------------------------------------------------------------------

u = x(4);
v = x(5);
r = x(6);
du = u-u0;

% -------------------------------------------------------------------------
% Factor of hydrodynamic coefficient to get force or moment
% -------------------------------------------------------------------------

f = zeros(46,1);
f(1)  = 1;
f(2)  = du;
f(3)  = du^2;
f(4)  = du^3;
f(5)  = 0; % acceleration up not needed
f(6)  = 0; % acceleration upuu not needed
f(7)  = v;
f(8)  = v^2;
f(9)  = v^3;
f(10) = v*abs(v);
f(11) = 0; % acceleration vp not needed
f(12) = 0; % acceleration vpvv not needed
f(13) = r;
f(14) = r^2;
f(15) = r^3;
f(16) = r*abs(r);
f(17) = 0; % acceleration rp not needed
f(18) = 0; % acceleration rprr not needed
f(19) = delta;
f(20) = delta^2;
f(21) = delta^3;
f(22) = delta^4;
f(23) = delta^5;
f(24) = delta*abs(delta);
f(25) = v*du;
f(26) = v*du^2;
f(27) = v^2*du;
f(28) = v^3*du;
f(29) = r*du;
f(30) = r*du^2;
f(31) = r^2*du;
f(32) = r^3*du;
f(33) = delta*du;
f(34) = delta*du^2;
f(35) = delta^2*du;
f(36) = delta^3*du;
f(37) = v*r;
f(38) = v*r^2;
f(39) = v^2*r;
f(40) = v^3*r;
f(41) = v*delta;
f(42) = v*delta^2;
f(43) = v^2*delta;
f(44) = r*delta;
f(45) = r*delta^2;
f(46) = r^2*delta;

% -------------------------------------------------------------------------
% Correction from Wolff
% only values unequal 1 are modified
% -------------------------------------------------------------------------

k = ones(46,1);
if (wolff == 1)    
    k(1)  = u^2;
    k(2)  = u;
    k(4)  = u^-1;
    k(7)  = u;
    k(9)  = u^-1;
    k(12) = u^-2;
    k(13) = u;
    k(15) = u^-1;
    k(18) = u^-2;
    k(19) = u^2;
    k(20) = u^2;
    k(21) = u^2;
    k(22) = u^2;
    k(23) = u^2;
    k(24) = u^2;
    k(26) = u^-1;
    k(27) = u^-1;
    k(28) = u^-2;
    k(30) = u^-1;
    k(31) = u^-1;
    k(32) = u^-2;
    k(33) = u;
    k(35) = u;
    k(36) = u;
    k(38) = u^-1;
    k(39) = u^-1;
    k(40) = u^-2;
    k(41) = u;
    k(42) = u;
    k(44) = u;
    k(45) = u;
end

f = f .* k;

% -------------------------------------------------------------------------
% Calculate hydrodynamic loads
% -------------------------------------------------------------------------

hydrodynLoads(1) = f' * hydroCoeff(:,1);
hydrodynLoads(2) = f' * hydroCoeff(:,2);
hydrodynLoads(3) = f' * hydroCoeff(:,3);

end
