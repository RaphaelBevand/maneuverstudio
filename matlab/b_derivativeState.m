function xp = b_derivativeState(x, hydrodynLoads, u0, M, xg, Izz, hydroCoeff, wolff)
% Function to calculate the time derivative of the state vector.
%
%   Function reads in the following parameters:
%      - state vector: state = [x0, y0, psi, u, v, r]
%      - hydrodynamic loads: hydroLoads = [X, Y, N]
%      - ship mass: M
%      - longitudinal center of gravity: xg
%      - mass moment of inertia around vertical axis: Izz
%      - hydrodynamic coefficients: coeff = [CX, CY, CN]
%
%   Function returns the time derivative of the state vector velocity
%   components:
%      - time derivative: xp = d( [x0, y0, psi, u, v, r] ) / dt

% -------------------------------------------------------------------------
% Pass specific arguments to local variables
% -------------------------------------------------------------------------

% Velocities
psi = x(3);
u = x(4);
v = x(5);
r = x(6);
du = u - u0;
% Forces
X = hydrodynLoads(1);
Y = hydrodynLoads(2);
N = hydrodynLoads(3);

% -------------------------------------------------------------------------
% Solve equation system to get ship acceleration
% INFO ---> Coefficients proportional to accelerations
% up, up*u^2, vp, vp*v^2, rp, rp*r^2 are:
% hydroCoeff(5,:) hydroCoeff(6,:) hydroCoeff(11,:)
% hydroCoeff(12,:) hydroCoeff(17,:) hydroCoeff(18,:)
% -------------------------------------------------------------------------
if wolff == true
    k = u^2;
else
    k = 1;
end

% Get yp and rp with cramer's rule
a11 = M - hydroCoeff(11,2) - hydroCoeff(12,2)*v^2/k;
a12 = M*xg - hydroCoeff(17,2) - hydroCoeff(18,2)*r^2/k;
a21 = M*xg - hydroCoeff(11,3) - hydroCoeff(12,3)*v^2/k;
a22 = Izz - hydroCoeff(17,3) - hydroCoeff(18,3)*r^2/k;


    
b1 = -M*u*r + Y;
b2 = -M*xg*u*r + N;

detA = a11 * a22 - a21 * a12;
detA1 = b1 * a22 - b2 * a12;
detA2 = a11 * b2 - a21 * b1;

% Get xp
xp(6) = detA2 / detA;
xp(5) = detA1 / detA;
xp(4) = (X + M*v*r + M*xg*r^2) / (M - hydroCoeff(5,1) - hydroCoeff(6,1)*du^2/k);

% -------------------------------------------------------------------------
% Calculate remaining components of state vector derivative
% -------------------------------------------------------------------------

% Derivative of course angle
xp(3) = r;

% Derivative of y0-position in global system
xp(2) = u*sin(psi) + v*cos(psi);

% Derivative of x0-position in global system
xp(1) = u*cos(psi) - v*sin(psi);

end