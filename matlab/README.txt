# Program for Simulation of Ship Manoevures
------------

This program was a work part of the lecture *manoeuvrability of ships*.

The program is based on the approach of hydrodynamic coefficients to compute the forces acting on the ship.

It is possible to simulate the following manoeuvres:
* Turning circle and
* Zigzag

The following ship types are supported:
* Tanker,
* Series60,
* Containership,
* Ferry.

The ship dimensions and properties as well the hydrodynamic coefficients are taken from model tests [1].

A documentation of the program and task regarding ship manoeuvrability was part of this program.

Significant manoeuver results will be computed and printed on screen.
It is possible to plot simulation results within Matlab and/or write simulation data to disk for post-processing.

To start the program must execute the main program ```manoeuvre_main.m```.
All functions/subroutines must be placed in the same folder as the main program.

The following variables can be modified to run diffferent manoeuvre types:

```
manoeuvre   = 1;            % 1: turning circle, 2: zigzag
ship        = 1;            % 1: tanker, 2: series60
                            % 3: container ship, 4: ferry
deltaMax    = 5*pi/180;     % target rudder angle (in rad)
psiMax      = 10*pi/180;    % target course angle (in rad)
                            % psiMax is only required for zigzag
maxOrder    = 5;            % 1-5: Maximum order of coefficients
rho         = 1025;         % water density [kg/m^3]
timestep    = 1e-4;         % dimensionless timestep, dimensionless!
wolff       = true;         % true/false, correction factors by Wolff
plotgraphs  = 1;            % plot graphical output
postprocess = 1;            % write simulation data and results on disk
```

A more detailed explanation is part of the documentation.

[1]  https://tubdok.tub.tuhh.de/handle/11420/843
