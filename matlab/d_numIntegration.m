function x = d_numIntegration(dt, x, xp)
% Function to integrate the state derivative xp numerically.
%   Numerical integration is carried out with first order euler scheme.

for row = 1:6
    x(row) = x(row) + dt * xp(row);
end
end
