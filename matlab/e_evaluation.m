function simulation_results = e_evaluation( data_manoeuvre, U0, time, state, xp, beta, delta )
% Function to evaluate the simulation data of computation and 
% compute significant values of manoeuver.

manoeuvre = data_manoeuvre(1);
deltaMax = data_manoeuvre(2) * 180/pi;
psiMax   = data_manoeuvre(3) * 180/pi;

switch manoeuvre
    case 1 % Turning circle

        for i = 1:length(state)
            if abs(state(i,3)) < 90 && abs(state(i+1,3)) > 90
                t90 = time(i);
                x90 = state(i,1);
                y90 = state(i,2);
            elseif abs(state(i,3))< 180 && abs(state(i+1,3)) > 180 
                t180 = time(i);
                y180 = abs(state(i,2)); % tactical diameter
                break;
            elseif ( i == length(time) )
                error('Schiff dreht sich nicht ausreichend')
            end
        end

        r_stat = state(end,6);
        R_stat = abs( sqrt( state(end,4)^2 + state(end,5)^2 ) / (r_stat*pi/180 ) );
        X_pivot = R_stat * sin(beta(end)*pi/180);
        t_delay = time(end) + (0 - state(end,3))/r_stat;
        u_red = state(end,4)/U0;
        
        delta_stat = delta(end);
        xmax = max(state(:,1));

        if max(state(:,2)) > abs(min(state(:,2)))
            ymax = max(state(:,2));
        else
            ymax = min(state(:,2));
        end

        beta_stat = beta(end);
        u_end = state(end,4)*3600/1852;

        simulation_results = [delta_stat x90 y90 xmax y180 t90 t180 ymax R_stat beta_stat r_stat u_end u_red];
        
        fprintf('Ergebnisse des Drehkreis Manövers:\n')
        fprintf('--------------------------------------------------\n');
        fprintf('Zeit Manoever  : %15.3f s  %15.3f min\n',time(end),time(end)/60)
        fprintf('x0(psi=90)     : %15.1f m \n',x90)
        fprintf('y0(psi=90)     : %15.1f m \n',y90)
        fprintf('Maximum x0     : %15.1f m \n',xmax)
        fprintf('y(psi=180)     : %15.1f m \n',y180)
        fprintf('Zeit(psi=90)   : %15.0f s \n',t90)
        fprintf('Zeit(psi=180)  : %15.0f s \n',t180)
        fprintf('Maximum y0     : %15.1f m \n',ymax)
        fprintf('Radius_stat    : %15.1f m \n',R_stat)
        fprintf('beta_stat      : %14.2f Grad/s \n',beta_stat)
        fprintf('r_stat         : %15.3f Grad/s \n',r_stat)
        fprintf('u_end          : %15.2f kn \n', u_end)
        fprintf('u_red          : %15.3f \n\n\n',u_red)
        fprintf('X_pivot        : %15.3f m \n',X_pivot)
        fprintf('t_delay        : %15.3f s \n',t_delay)

    case 2 % Zig-Zag

        pts = length(state(:,1));
        
        % Anschwenkzeit Ta --- erstes Mal Psi_max ueberschreiten
        for i=1:pts-1
            if abs(state(i,3)) <= abs(psiMax) && abs(psiMax) <= abs(state(i+1,3))
                Ta = (time(i) + time(i+1)) / 2;
                iT = i;
                iTa = i;
                break;
            else
                Ta = 0;
            end
        end
   
        % Stuetzzeit Tb --- Drehung aufhalten
        for i=iT:pts-1       
            if sign(state(i,6)) ~= sign(state(i+1,6))
                Tb = (time(i) + time(i+1)) / 2 - Ta;
                iT = i;
                break;
            else
                Tb = 0;
            end
        end

        % Ausweichzeit Tt, Nulldurchgang Psi
        for i=iT:pts-1        
            if sign(state(i,3)) ~= sign(state(i+1,3))
                Tt = (time(i) + time(i+1)) / 2;
                iT = i;
                break;
            else
                Tt = 0;
            end
        end

        % Rueckdrehzeit Tc
        Tc = Tt - Ta - Tb;

        % Periodendauer
        count = 0;
        for i = iT+1:pts-1
            if sign(state(i,3)) ~= sign(state(i+1,3)) && count == 0
                % Ueberspringe ersten Nulldurchgang von psi nac Tt
                count = 1;
            elseif sign(state(i,3)) ~= sign(state(i+1,3)) && count ~= 0
                T = ((time(i) + time(i+1)) / 2) - Tt;
                break;
            end
        end

        extrema = 3;
        A0 = zeros(extrema,1);
        ymax = zeros(extrema,1);
        rmax = zeros(extrema,1);

        count = 1;
        for i=iTa:pts-1

            % Ueberschwingwinkel
            if count < extrema+1
                    if sign(state(i,6)) ~= sign(state(i+1,6))
                        % Drehrate aendert Vorzeichen --- Ueberschwingwinkel erreicht
                         A0(count) = (state(i,3) + state(i+1,3)) / 2 - ...
                             sign(state(i,3)) * abs(psiMax);
                        count = count + 1;
                    end
            else
                break;
            end
        end

        % Ableitungen der Bahnkurve
        d_y_dt = zeros( pts, 1 );
        for i=2:pts
            % y0 Ableitung
            d_y_dt(i) = ( state(i,2)-state(i-1,2) ) / ( time(i)-time(i-1) );
        end
        % y0 Ableitung zu Beginn
        d_y_dt(1) = ( state(2,2)-state(1,2) ) / ( time(2)-time(1) );        

        count = 1;  
        for i=iTa:pts-1
            % Transfer - Extrema
            if count < extrema+1
                if sign(d_y_dt(i)) ~= sign(d_y_dt(i+1))
                    ymax(count) = (state(i,2) + state(i+1,2)) / 2;
                    count = count + 1;
                end
            else
                break;
            end
        end

        count = 1;
        for i=iTa:pts-1
            % Drehgeschw. - Extrema
            if count < extrema+1
                if sign(xp(i,3)) ~= sign(xp(i+1,3))
                    rmax(count) = (state(i,6) + state(i+1,6)) / 2;
                    count = count + 1;
                end
            else
                break;
            end
        end

        fprintf('Ergebnisse des Zickzack Manövers:\n')
        fprintf('--------------------------------------------------\n');
        fprintf('Zeit Manoever : %15.1f s  %15.3f min\n',time(end),time(end)/60)
        fprintf('Anschwenkzeit : %15.1f s \n',Ta)
        fprintf('Stuetzzeit    : %15.1f s \n',Tb)
        fprintf('Rueckdrehzeit : %15.1f s \n',Tc)
        fprintf('Ausweichzeit  : %15.1f s \n',Tt)
        fprintf('Periode       : %15.1f s \n',T)
        fprintf('\nUeberschwingwinkel:\n')
        fprintf('A01    : %15.2f Grad \n',A0(1))
        fprintf('A02    : %15.2f Grad \n',A0(2))
        fprintf('A03    : %15.2f Grad \n',A0(3))
        fprintf('\nTransfer-Maxima:\n')
        fprintf('Y0MAX1 : %15.1f m \n',ymax(1))
        fprintf('Y0MAX2 : %15.1f m \n',ymax(2))
        fprintf('Y0MAX3 : %15.1f m \n',ymax(3))
        fprintf('\nDrehrate-Maxima:\n')
        fprintf('RMAX1  : %15.3f Grad/s \n',rmax(1))
        fprintf('RMAX2  : %15.3f Grad/s \n',rmax(2))
        fprintf('RMAX3  : %15.3f Grad/s \n',rmax(3))

        simulation_results = [ deltaMax psiMax Ta Tb Tc Tt T A0' ymax' rmax' ];
end
end
