function [ delta, zzstat ] = c_rudder(data_manoeuvre, x, dt, delta, deltaRate, zzstat)
% Function to adapt the rudder angle.
%
%   Function reads in the following parameters:
%      - manoeuver parameters: data_manoeuvre = [manoeuvre, deltaMax, psiMax]
%      - state vector: state = [x0, y0, psi, u, v, r]
%      - time increment: dt
%      - current rudder angle: delta
%      - rudder turn velocity: deltaRate
%      - manoever status needed for zig-zag: zzstat
%
%  Function returns the rudder angle for the next time step according to
%  the manoeuver conditions found in the state vector.

% -------------------------------------------------------------------------
% Pass specific arguments to local variables
% -------------------------------------------------------------------------

manoeuvre = data_manoeuvre(1);
deltaMax = data_manoeuvre(2);
psiMax   = data_manoeuvre(3);

psi = x(3);
r = x(6);

% -------------------------------------------------------------------------
% Turn the rudder
% -------------------------------------------------------------------------

switch manoeuvre

    case (1) % turning circle

        if abs(delta) < abs(deltaMax)
            delta = delta + dt * deltaRate * sign(deltaMax);
        else
            delta = deltaMax;
        end

    case (2) % zigzag

        if deltaMax > 0
        if zzstat == 0
            if delta < deltaMax
                delta = delta + dt * deltaRate;
            else
                delta = deltaMax;
            end
            if abs(psiMax) <= -psi && r < 0
                zzstat = 1;
            else
                zzstat = 0;
            end
        else % zzstat == 1
            if delta > -deltaMax
                delta = delta + dt * -deltaRate;
            else
                delta = -deltaMax;
            end
            if abs(psiMax) <= psi && r > 0
                zzstat = 0;
            else
                zzstat = 1;
            end
        end
        else % negative deltaMax, do the opposite
        if zzstat == 0
            if delta > deltaMax
                delta = delta + dt * -deltaRate;
            else
                delta = deltaMax;
            end
            if abs(psiMax) <= psi && r > 0
                zzstat = 1;
            else
                zzstat = 0;
            end
        else % zzstat == 1
            if delta < -deltaMax
                delta = delta + dt * deltaRate;
            else
                delta = -deltaMax;
            end
            if abs(psiMax) <= -psi && r < 0
                zzstat = 0;
            else
                zzstat = 1;
            end
        end
        end
end
end