%
% Routine for output of figures
%

% used layout:
% ---------------------
%   sub1 | sub2 | sub3 |
% ----------------------
%   sub4 | sub5 | sub6 |
% ----------------------
figure(1)

% -------------------------------------------------------------------------
% hydrodynamic forces
% -------------------------------------------------------------------------

subplot(2,3,1);
hold on
grid on
plot(time,xyn)
legend('X''','Y''','N''','Location','ne')
xlabel('Time'), ylabel('Hydrodyn. Load')

% -------------------------------------------------------------------------
% course angle, delta, beta, turning rate
% -------------------------------------------------------------------------

subplot(2,3,4);
hold on
grid on
plot(time,[x(:,3),delta,beta,x(:,6)])
legend('Psi','delta','beta','r','Location','ne')
xlabel('Time'), ylabel('Angle')

% -------------------------------------------------------------------------
% global course and positions
% -------------------------------------------------------------------------

subplot(2,3,[2,3,5,6]);
hold on
grid on
plot(x(:,2),x(:,1))
xlabel('y0'), ylabel('x0')
