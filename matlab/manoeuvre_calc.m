function [ simulation_success, x, xp, xyn, delta, beta, time ] = ...
    manoeuvre_calc( hydroCoeff, data_manoeuvre, data_ship, dt, file_simulation_data )


% -------------------------------------------------------------------------
% Pass specific arguments to local variables
% -------------------------------------------------------------------------

manoeuvre = data_manoeuvre(1);
deltaMax = data_manoeuvre(2);
psiMax   = data_manoeuvre(3);

M        = data_ship(1);
xg       = data_ship(2);
Izz      = data_ship(3);
delta0   = data_ship(4); % neutral rudder angle
u0       = data_ship(5);
dr       = data_ship(6); % rudder turning rate
wolff    = data_ship(7); % correction of wolff

% -------------------------------------------------------------------------
% Prepare temporary simulation output files
% -------------------------------------------------------------------------

% Write header for simulation output to file
header = {'x0' 'y0' 'psi' 'u' 'v' 'r' 'up' 'vp' 'rp' 'X' 'Y' 'N' 'delta' 'beta' 'time'};
fmt = repmat('%s, ', 1, length(header));
fmt(end:end+1) = '\n';
fid = fopen(file_simulation_data, 'w');
fprintf(fid, fmt, header{:});

% -------------------------------------------------------------------------
% Time simulation
% -------------------------------------------------------------------------

% Initial conditions
t = 0;
delta = delta0;
beta = 0;
x = [0, 0, 0, u0, 0, 0];
xyn = zeros(1,3);
simulation_finished = false;
simulation_success = true;
step = 1;
zzstat = 0; % zigzag status
cross0 = 0; % count zero crossing of psi angle (end condition for zig-zag)
modstep = 1/dt/50; % 50 points per dimensionless time unit are saved

% Time loop
while simulation_finished == false

    % Calculate values
    beta = atan( -x(5) / x(4) );
    xyn = a_hydrodynLoads(x, u0, delta, hydroCoeff, wolff);
    xp = b_derivativeState(x, xyn, u0, M, xg, Izz, hydroCoeff, wolff);
    [delta, zzstat] = c_rudder(data_manoeuvre, x, dt, delta, dr, zzstat);
    x = d_numIntegration(dt, x, xp);

    % Store verification values if it is a zig-zag manoeuvre
    if manoeuvre == 2
       if mod(step,2) == 1
           psi1 = x(3);
           if step == 1
               psi2 = psi1;
           end
       elseif mod(step,2) == 0
           psi2 = x(3);
       end
    end

    % Primary end condition of simulation
    switch manoeuvre
        case 1 % Turning circle
            if abs(x(3)) >= 2.5 * 360*pi/180
                simulation_finished = true;
                fprintf('--------------------------------------------------\n');
                fprintf('Simulation nach 2.5 Drehkreisen beendet.\n')
                fprintf('--------------------------------------------------\n');
            end
        case 2 % Zig-Zag
             % count cero crossings of course angle
             if sign(psi1) ~= sign(psi2)
                  cross0 = cross0 + 1;
                  % end simulation after 3 zig-zags
                  if cross0 == 7
                      simulation_finished = true;
                      fprintf('--------------------------------------------------\n');
                      fprintf('Simulation nach 3 Zickzacks beendet.\n')
                      fprintf('--------------------------------------------------\n');
                  end
             end
    end

    % Secondary end condition of manoeuvre (if primary end condition is not met)
    if t >= 150
        simulation_finished = true;
        simulation_success = false;
        fprintf('--------------------------------------------------\n');
        disp('!!! Simulation agebrochen !!!')
        disp('Primaere Abbruchkriterien: ')
        disp('   - Drehkreis: psi > 900 Grad (2.5*360)')
        disp('   - Zickzack : zero crossings von psi < 7')
        fprintf('nach %15.0f Schleifendurchlaeufen nicht erfuellt!\n', step)
        fprintf('--------------------------------------------------\n');
    end

    % Write to output file
    if wolff == true
        k = x(4)^2;
    else
        k = 1;
    end
    
    xyn_all(1) = xyn(1)+hydroCoeff(5,1)*xp(4) + hydroCoeff(6,1)*xp(4)*(x(4)-u0)^2/k;

    xyn_all(2) = xyn(2)+ hydroCoeff(11,2)*xp(5) + hydroCoeff(12,2)*xp(5)*x(5)^2/k + ...
        hydroCoeff(17,2)*xp(6) - hydroCoeff(18,2)*xp(6)*x(6)^2/k;
    xyn_all(3) = xyn(3)+ hydroCoeff(11,3)*xp(5) + hydroCoeff(12,3)*xp(5)*x(5)^2/k + ...
        hydroCoeff(17,3)*xp(6) - hydroCoeff(18,3)*xp(6)*x(6)^2/k;

%     if mod(step,modstep) == 0 || step == 1
        simulation_data = [x, xp(4:6), xyn_all, delta, beta, t];
        dlmwrite(file_simulation_data,simulation_data,'-append','delimiter',',');
%     end

    % Next time step
    step = step + 1;
    t = t + dt;
end

% Write last time step to output file if not written
if mod(step,modstep) ~= 0
    simulation_data = [x, xp(4:6), xyn_all, delta, beta, t];
    dlmwrite(file_simulation_data,simulation_data,'-append','delimiter',',');
end

% -------------------------------------------------------------------------
% Write results from file to output arrays
% -------------------------------------------------------------------------

% Save csv-data to matrix, pass to output
simulation_data = csvread(file_simulation_data,1,0);
x = simulation_data(:,1:6); 
xp = simulation_data(:,7:9);
xyn = simulation_data(:,10:12);
delta = simulation_data(:,13);
beta = simulation_data(:,14);
time = simulation_data(:,15);

% Change angle back to degrees
x(:,[3 6]) = x(:,[3 6]) * 180/pi;
xp(:,3) = xp(:,3) * 180/pi;
delta = delta * 180/pi;
beta = beta * 180/pi;

% % Delete the simulation output file
% if exist(file_simulation_data, 'file') ~= 0
%     delete(file_simulation_data)
% end

end
