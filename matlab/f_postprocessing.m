%
% Post processing routine.
% Write simulation data into files
%
fprintf('\n--------------------------------------------------\n')

switch manoeuvre
    case 1
      manoeuvrename='drehkreis';
    case 2
      manoeuvrename='zickzack';
end
switch ship
    case 1
        shipname='tanker';
    case 2
        shipname='serie60';
    case 3
        shipname='container';
    case 4
        shipname='faehre';
end

if simulation_success == true
    % Write results of manoeuvre to file
    fprintf('Simulationsergebnisse in Datei:\n')
    switch manoeuvre
    case 1
		filename=strcat('results/',shipname,'_',manoeuvrename,...
			'_ordnung_',num2str(maxOrder),'.dat');
		fid = fopen(filename,'a');
		fmt = strcat(repmat('%.1f, ',1,5),' %.0f, %.0f, %.1f, %.1f, ',repmat('%.2f, ',1,4));
		fmt(end:end+1) = '\n';
		fprintf(fid, '%s %s\n', '% Schiff:', shipname);
		fprintf(fid, '%s %s\n', '% Maneuver:', manoeuvrename);
		fprintf(fid, '%s\n', '% (1) delta');
		fprintf(fid, '%s\n', '% (2) x90');
		fprintf(fid, '%s\n', '% (3) y90');
		fprintf(fid, '%s\n', '% (4) x_max');
		fprintf(fid, '%s\n', '% (5) d_takt');
		fprintf(fid, '%s\n', '% (6) t90');
		fprintf(fid, '%s\n', '% (7) t_180');
		fprintf(fid, '%s\n', '% (8) y_max');
		fprintf(fid, '%s\n', '% (9) R_stat');
		fprintf(fid, '%s\n', '% (10) beta');
		fprintf(fid, '%s\n', '% (11) r_stat');
		fprintf(fid, '%s\n', '% (12) u_end');
		fprintf(fid, '%s\n', '% (13) u/u0');
		fprintf(fid, fmt, simulation_results);
		fclose(fid);
   case 2
		% Schreibe 'Ergebnisse' der Manöversimulation
		filename=strcat('results/',shipname,'_',manoeuvrename,...
				'_ordnung_',num2str(maxOrder),'.dat');
		fid = fopen(filename,'a');
		fmt = strcat(repmat('%.1f ,',1,7),' %.2f, %.2f, %.2f, %.1f, %.1f, %.1f, ',repmat('%.2f, ',1,3));
		fmt(end:end+1) = '\n';
		fprintf(fid, '%s %s\n', '% Schiff:', shipname);
		fprintf(fid, '%s %s\n', '% Maneuver:', manoeuvrename);
		fprintf(fid, '%s\n', '% (1) delta');
		fprintf(fid, '%s\n', '% (2) psi');
		fprintf(fid, '%s\n', '% (3) Ta');
		fprintf(fid, '%s\n', '% (4) Tb');
		fprintf(fid, '%s\n', '% (5) Tc');
		fprintf(fid, '%s\n', '% (6) Tt');
		fprintf(fid, '%s\n', '% (7) T');
		fprintf(fid, '%s\n', '% (8) A01');
		fprintf(fid, '%s\n', '% (9) A02');
		fprintf(fid, '%s\n', '% (10) A03');
		fprintf(fid, '%s\n', '% (11) Y0MAX1');
		fprintf(fid, '%s\n', '% (12) Y0MAX2');
		fprintf(fid, '%s\n', '% (13) Y0MAX3');
		fprintf(fid, '%s\n', '% (14) RMAX1');
		fprintf(fid, '%s\n', '% (15) RMAX2');
		fprintf(fid, '%s\n', '% (16) RMAX3');
		fprintf(fid, fmt, simulation_results);
		fclose(fid);
    end
    fprintf('%s\n', filename)
else
    fprintf('Keine Auswertung des Manövers erfolgt und\n')
    fprintf('Auswertungsergebnisse nicht in Datei exportiert!\n')
end

fprintf('Simulationsdaten in Datei:\n')
switch manoeuvre
    case 1
    filename=strcat('results/',shipname,'_',manoeuvrename,...
    '_deltamax_',num2str(deltaMax*180/pi),...
    '_ordnung_',num2str(maxOrder),'.dat');
    case 2
    filename=strcat('results/',shipname,'_',manoeuvrename,...
    '_deltamax_',num2str(deltaMax*180/pi),...
    '_psimax_',num2str(psiMax*180/pi),...
    '_ordnung_',num2str(maxOrder),'.dat');
end
fprintf('%s\n', filename)

fid = fopen(filename,'w');
%fmt = strcat(repmat('%.3f, ', 1, 12), ' %e, %e, %e');
fmt = repmat('%e, ', 1, 15);
fmt(end:end+1) = '\n';
fprintf(fid, '%s %s\n', '% Schiff:', shipname);
fprintf(fid, '%s %s\n', '% Maneuver:', manoeuvrename);
fprintf(fid, '%s %f\n', '% DeltaMax:', deltaMax*180/pi);
if manoeuvre == 2
    fprintf(fid, '%s %f\n', '% PsiMax:', psiMax*180/pi);
end
fprintf(fid, '%s\n', '% (1) Zeit');
fprintf(fid, '%s\n', '% (2) x0');
fprintf(fid, '%s\n', '% (3) y0');
fprintf(fid, '%s\n', '% (4) psi');
fprintf(fid, '%s\n', '% (5) u');
fprintf(fid, '%s\n', '% (6) v');
fprintf(fid, '%s\n', '% (7) r');
fprintf(fid, '%s\n', '% (8) up');
fprintf(fid, '%s\n', '% (9) vp');
fprintf(fid, '%s\n', '% (10) rp');
fprintf(fid, '%s\n', '% (11) delta');
fprintf(fid, '%s\n', '% (12) beta');
fprintf(fid, '%s\n', '% (13) X (dimensionless)');
fprintf(fid, '%s\n', '% (14) Y (dimensionless)');
fprintf(fid, '%s\n', '% (15) N (dimensionless)');
fprintf(fid, fmt, [time x(:,1:6) xp(:,1:3) delta beta xyn(:,1:3)]');
fclose(fid);