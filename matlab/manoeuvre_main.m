%%%%
%
% Program for Simulation of Ship Manoeuvres
%
% 03.02.2017
%
% Authors:
% 	robert.beckmann@tuhh.de
% 	raphael.bevand@tuhh.de
%
% Available manoeuvres: 
% 	turning circle and zigzag 
% 
% Available ship types:
%	tanker, series60, containership, ferry
%
% For more details see README.md
% 
%%%


clear
clc

% -------------------------------------------------------------------------
% User input.
% -------------------------------------------------------------------------

manoeuvre   = 1;            % 1: turning circle, 2: zigzag
ship        = 3;            % 1: tanker, 2: series60
                            % 3: container ship, 4: ferry
deltaMax    = -20*pi/180;     % target rudder angle (in rad)
psiMax      = 10*pi/180;    % target course angle (in rad)
                            % psiMax is only required for zigzag
maxOrder    = 5;            % 1-5: Maximum order of coefficients
rho         = 1025;         % water density [kg/m^3]
timestep    = 1e-3;         % dimensionless timestep, dimensionless!
wolff       = true;         % true/false, correction factors by Wolff
plotgraphs  = 1;            % plot graphical output
postprocess = 1;            % write simulation data and results on disk

% -------------------------------------------------------------------------
% Print user input to display for verification/check.
% -------------------------------------------------------------------------

fprintf('--------------------------------------------------\n');
fprintf('Simulationsdaten:\n')
fprintf('--------------------------------------------------\n');
switch ship
    case 1
        fprintf('Schiff    : Tanker    \n')
    case 2
        fprintf('Schiff    : Serie60   \n')
    case 3
        fprintf('Schiff    : Container \n')
    case 4
        fprintf('Schiff    : Faehre    \n')
    otherwise
        error('Schiff    : UNBEKANNT')
end
switch manoeuvre
    case 1
        fprintf('Manoever  : Drehkreis   \n')
        fprintf('DeltaMax  : %4.3f deg   \n',deltaMax*180/pi)
    case 2
        fprintf('Manoever  : Zick-Zack   \n')
        fprintf('DeltaMax  : %4.3f deg   \n',deltaMax*180/pi)
        fprintf('PsiMax    : %4.3f deg   \n',psiMax*180/pi)
    otherwise
        error('Manoever    : UNBEKANNT')
end
switch wolff
    case 0
        fprintf('Korrektur : Nein   \n' )        
    case 1
        fprintf('Korrektur : Ja   \n' )
end
if maxOrder < 0 || maxOrder > 5
    error('maxOrdnung: UNZULAESSIG')
else
    fprintf('maxOrdnung: %i   \n\n',maxOrder)
end

% -------------------------------------------------------------------------
% Import ship parameters (speed, length, mass, ...) and dimensionless 
% hydrodynamic coefficients X,Y,N.
% The simulation is computed in dimensionless space, therefore convert all 
% parameters to dimensionless ones.
% -------------------------------------------------------------------------
% import ship parameters
shipParam = csvread('tables/shipParam.csv',7+ship, 1,[7+ship 1 7+ship 7]);
% import hydrodynamic coefficients
start = 9 + (ship-1)*3;
ende  = start + 2; 
hydroCoeff = csvread('tables/hydroCoef.csv',3, start,[3, start, 48, ende])*10^-6;
hydroCoeff_idx = csvread('tables/hydroCoef.csv',3, 1,[3 1 48 8]);
% set high order coeff. to zero (if required)
hydroCoeff(hydroCoeff_idx(:,2)>maxOrder,:) = 0;

% compute dimensionless values
M_dash = shipParam(1)*10^-5;
Mxg_dash = shipParam(2)*10^-5;
xg_dash = Mxg_dash/M_dash;
Izz_dash = shipParam(3)*10^-5;
L = shipParam(4);
delta0 = shipParam(5)*pi/180;
U0 = shipParam(6)*1852/3600;
deltaRate = shipParam(7)*pi/180*L/U0;

% -------------------------------------------------------------------------
% Print ship properties to display.
% -------------------------------------------------------------------------

fprintf('--------------------------------------------------\n');
fprintf('Schiffsdaten:\n')
fprintf('--------------------------------------------------\n');
fprintf('rho       : %15.3f t/m3  \n',rho/1000)
fprintf('L         : %15.3f m     \n',L)
fprintf('M         : %15.3f t     \n',M_dash*0.5*rho*L^3/1000)
fprintf('XG        : %15.3f m     \n',xg_dash*L)
fprintf('Izz       : %15.3E tm2   \n',M_dash*0.5*rho*L^5/1000)
fprintf('delta0    : %15.3f deg   \n',delta0*180/pi)
fprintf('deltaRate : %15.3f deg/s \n',deltaRate*180/pi/L*U0)
fprintf('U0        : %15.3f m/s %15.3f kn   \n\n',U0, U0/1852*3600)

% -------------------------------------------------------------------------
% Pass simulation data to the function manoeuvre_calc to carry out
% simulation.
% The simulation data is written to file and after computation read from
% file for analysis and post processing.
% -------------------------------------------------------------------------

file_simulation_data = 'results/simulation-data.tmp';
if exist('results/', 'dir') == 0
    mkdir('results/');
end
% generate vector of manoeuvre data
data_manoeuvre = ...
    [ manoeuvre, deltaMax, psiMax ];
% generate vector of ship data
data_ship = ...
    [ M_dash xg_dash Izz_dash delta0 U0/U0 deltaRate wolff ];

[ simulation_success, x, xp, xyn, delta, beta, time ] = ...
    manoeuvre_calc( hydroCoeff, data_manoeuvre, data_ship, timestep, file_simulation_data );

% -------------------------------------------------------------------------
% Simulation data and post processing.
% -------------------------------------------------------------------------

% compute dimensional values
x(:,[1 2]) = x(:,[1 2]) * L; % x0, y0 [m]
x(:,[4 5]) = x(:,[4 5]) * U0; % u, v [m/s]
x(:,6) = x(:,6) * U0/L; % r [deg/s]
xp(:,[1 2]) = xp(:,[1 2]) * U0 * U0/L; % up, vp [m/s2]
xp(:,3) = xp(:,3) * (U0/L)^2; % rp [deg/s2]
time = time * L/U0; % time [s]

% analysis of simulation data
if simulation_success == true
    simulation_results = ...
        e_evaluation( data_manoeuvre, U0, time, x, xp, beta, delta );
end

% generate simulation data log files for post processing
if postprocess == 1
    f_postprocessing
end
if plotgraphs == 1
    g_plotgraphs
end
