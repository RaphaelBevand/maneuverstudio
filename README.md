# ManeuverStudio
Application with a graphical user interface to simulate nautical maneuvers.

Authors:
* Beckmann, R.
* Bevand, R.


The program is based on the approach of hydrodynamic coefficients to compute the
forces acting on the ship.

Implemented maneuvers:
* Turning circle and
* Zigzag

The following ship types are supported:
* Tanker,
* Series 60,
* Containership,
* Ferry.

The ship dimensions and properties as well the hydrodynamic coefficients are taken
from model tests [1]. Significant manoeuver results will be computed and printed
on screen. Simulation data is stored on disk for post-processing.


Literature:

[1]  https://tubdok.tub.tuhh.de/handle/11420/843


## Deploy using `pipenv`:

```
pip3 install pipenv

git clone https://gitlab.com/orkusinum/maneuverstudio.git
cd maneuverstudio

pipenv install
pipenv shell

./maneuverstudio [arguments]

# cleanup
pipenv --rm
```

Python `>= 3.5` is needed.

E.g. to enforce python 3.6:
```
pipenv install --python 3.6
```
